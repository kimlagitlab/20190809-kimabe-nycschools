//
//  APIClient.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    static func ClientFetchSchool(query: String, completion:@escaping (Result<[SchoolElement]>)->Void) {
        Alamofire.request(APIRouter.fetchSchool(query: query))
            .responseDecodable { (response: DataResponse<[SchoolElement]>) in
                completion(response.result)
        }
    }
    
    static func ClientFetchSchoolList(query: String, completion:@escaping (Result<[SchoolElement]>)->Void) {
        Alamofire.request(APIRouter.fetchSchoolList(order: query))
            .responseDecodable { (response: DataResponse<[SchoolElement]>) in
                completion(response.result)
        }
    }
    
    static func ClientFetchSatScores(wherex: String, completion:@escaping (Result<[SatScoreElement]>)->Void) {
        Alamofire.request(APIRouter.fetchSatScores(wherex: wherex))
            .responseDecodable { (response: DataResponse<[SatScoreElement]>) in
                completion(response.result)
        }
    }
    
}
