//
//  APIRouter.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import Alamofire

enum APIRouter: APIConfiguration {
    
    case fetchSchool(query: String)
    case fetchSchoolList(order: String)
    case fetchSatScores(wherex: String)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .fetchSchool, .fetchSchoolList, .fetchSatScores:
            return .get
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .fetchSchool(_):
            return "/s3k6-pzi2.json"
        case .fetchSchoolList(_):
            return "/s3k6-pzi2.json"
        case .fetchSatScores(_):
            return "/f9bf-2cp4.json"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .fetchSchool(let query):
            return [Kilo.APIParameterKey.query : query]
        case .fetchSchoolList(let order):
            return [Kilo.APIParameterKey.order : order]
        case .fetchSatScores(let wherex):
            return [Kilo.APIParameterKey.wherex : wherex]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Kilo.ProductionServer.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        print("Request: \(urlRequest)")
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
                //urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                print("Parameters: \(parameters)")
                print(urlRequest)
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
