//
//  DetailsViewModel.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import UIKit



protocol DetailsViewModel {
    var satscores: [Int] { get }
    var delegate : DetailsViewControllerDelegate? { get set }
    
    func FetchSatScores(wherex: String)
}


class DetailsToDetailsViewController : DetailsViewModel {
    var satscores: [Int] = []
    var delegate: DetailsViewControllerDelegate?
    
    init() {}
    
    func FetchSatScores(wherex: String) {
        APIClient.ClientFetchSatScores(wherex: wherex, completion: { result in
            switch result {
            case .success(let resultado):
                if let satScoreElement = resultado.first {
                    self.delegate?.didFetchSatScores(satScore: satScoreElement)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
    
    
}
