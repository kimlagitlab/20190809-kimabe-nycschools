//
//  ListViewModel.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import UIKit

protocol ListViewModel {
    var listHeader: [String] { get }
    var listColor: [UIColor] { get }
    var list: [[SchoolElement]] { get }
    var delegate : ListViewControllerDelegate? { get set }
    
    func FetchList(query: String)
}

class ListToListViewController : ListViewModel {
    var listHeader: [String] = ["Bronx","Brooklyn","Queens","Staten Island", "Manhattan"]
    var listColor: [UIColor] = [boroughColor.bronx.color(),
                                boroughColor.brooklyn.color(),
                                boroughColor.queens.color(),
                                boroughColor.statenisland.color(),
                                boroughColor.manhattan.color()]
    var list: [[SchoolElement]] = [[],[],[],[],[]]
    var delegate: ListViewControllerDelegate?
    
    init() {}
    
    func FetchList(query: String) {
        APIClient.ClientFetchSchoolList(query: query, completion: { result in
            switch result {
            case .success(let resultado):
                for school in resultado {
                    switch school.boro {
                    case "X":
                        self.list[0].append(school)
                    case "K":
                        self.list[1].append(school)
                    case "Q":
                        self.list[2].append(school)
                    case "R":
                        self.list[3].append(school)
                    case "M":
                        self.list[4].append(school)
                    default:
                        break
                    }
                }
                
                self.delegate?.didFetchList(list: self.list, listHeader: self.listHeader, listColor: self.listColor)
            case .failure(let error):
                print(error.localizedDescription)
            }
        })
    }
    
}

