//
//  SchoolElement.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - SchoolElement
class SchoolElement: Codable {
    let dbn: String
    let schoolName: String
    let boro: String
    let overviewParagraph: String
    let academicopportunities1: String?
    let academicopportunities2: String?
    let academicopportunities3: String?
    let academicopportunities4: String?
    let academicopportunities5: String?
    let ellPrograms: String
    let languageClasses: String?
    let advancedplacementCourses: String?
    let neighborhood: String
    let sharedSpace: String?
    let buildingCode: String?
    let location: String
    let phoneNumber: String
    let faxNumber: String?
    let schoolEmail: String?
    let website: String
    let subway: String
    let bus: String
    let grades2018: String
    let finalgrades: String
    let totalStudents: String
    let startTime: String?
    let endTime: String?
    let addtlInfo1: String?
    let extracurricularActivities: String?
    let psalSportsBoys: String?
    let schoolSports: String?
    let graduationRate: String?
    let attendanceRate: String
    let pctStuEnoughVariety: String?
    let collegeCareerRate: String?
    let pctStuSafe: String?
    let schoolAccessibilityDescription: String?
    let offerRate1: String?
    let program1: String
    let code1: String
    let interest1: String
    let method1: String
    let seats9Ge1: String?
    let grade9Gefilledflag1: String?
    let grade9Geapplicants1: String?
    let seats9Swd1: String?
    let grade9Swdfilledflag1: String?
    let grade9Swdapplicants1: String?
    let seats101: String
    let admissionspriority11: String?
    let admissionspriority21: String?
    let admissionspriority31: String?
    let admissionspriority41: String?
    let admissionspriority51: String?
    let grade9Geapplicantsperseat1: String?
    let grade9Swdapplicantsperseat1: String?
    let primaryAddressLine1: String
    let city: String
    let zip: String
    let stateCode: String
    let latitude: String?
    let longitude: String?
    let communityBoard: String?
    let councilDistrict: String?
    let censusTract: String?
    let bin: String?
    let bbl: String?
    let nta: String?
    let borough: String?
    let school10ThSeats: String?
    let psalSportsGirls: String?
    let prgdesc1: String?
    let prgdesc2: String?
    let prgdesc3: String?
    let requirement13: String?
    let requirement23: String?
    let requirement33: String?
    let requirement43: String?
    let requirement53: String?
    let requirement63: String?
    let program2: String?
    let program3: String?
    let code2: String?
    let code3: String?
    let interest2: String?
    let interest3: String?
    let method2: String?
    let method3: String?
    let seats9Ge2: String?
    let seats9Ge3: String?
    let grade9Gefilledflag2: String?
    let grade9Gefilledflag3: String?
    let grade9Geapplicants2: String?
    let grade9Geapplicants3: String?
    let seats9Swd2: String?
    let seats9Swd3: String?
    let grade9Swdfilledflag2: String?
    let grade9Swdfilledflag3: String?
    let grade9Swdapplicants2: String?
    let grade9Swdapplicants3: String?
    let seats102: String?
    let seats103: String?
    let admissionspriority13: String?
    let eligibility2: String?
    let grade9Geapplicantsperseat2: String?
    let grade9Geapplicantsperseat3: String?
    let grade9Swdapplicantsperseat2: String?
    let grade9Swdapplicantsperseat3: String?
    let diplomaendorsements: String?
    let pbat: String?
    let requirement11: String?
    let requirement21: String?
    let requirement31: String?
    let requirement41: String?
    let requirement51: String?
    let directions1: String?
    let earlycollege: String?
    let psalSportsCoed: String?
    let campusName: String?
    let ptech: String?
    let admissionspriority61: String?
    let girls: String?
    let offerRate2: String?
    let admissionspriority12: String?
    let admissionspriority22: String?
    let admissionspriority32: String?
    let admissionspriority42: String?
    let eligibility1: String?
    let requirement12: String?
    let auditioninformation1: String?
    let auditioninformation2: String?
    let commonAudition1: String?
    let commonAudition2: String?
    let requirement22: String?
    let requirement32: String?
    let prgdesc4: String?
    let requirement14: String?
    let requirement24: String?
    let requirement34: String?
    let requirement42: String?
    let requirement44: String?
    let offerRate3: String?
    let offerRate4: String?
    let program4: String?
    let code4: String?
    let interest4: String?
    let method4: String?
    let seats9Ge4: String?
    let grade9Gefilledflag4: String?
    let grade9Geapplicants4: String?
    let seats9Swd4: String?
    let grade9Swdfilledflag4: String?
    let grade9Swdapplicants4: String?
    let seats104: String?
    let admissionspriority14: String?
    let admissionspriority23: String?
    let admissionspriority24: String?
    let auditioninformation3: String?
    let auditioninformation4: String?
    let grade9Geapplicantsperseat4: String?
    let grade9Swdapplicantsperseat4: String?
    let transfer: String?
    let international: String?
    let specialized: String?
    let seats1Specialized: String?
    let applicants1Specialized: String?
    let appperseat1Specialized: String?
    let prgdesc5: String?
    let requirement15: String?
    let requirement25: String?
    let requirement35: String?
    let requirement45: String?
    let requirement52: String?
    let requirement54: String?
    let requirement55: String?
    let program5: String?
    let code5: String?
    let interest5: String?
    let method5: String?
    let seats9Ge5: String?
    let grade9Gefilledflag5: String?
    let grade9Geapplicants5: String?
    let seats9Swd5: String?
    let grade9Swdfilledflag5: String?
    let grade9Swdapplicants5: String?
    let seats105: String?
    let admissionspriority15: String?
    let auditioninformation5: String?
    let commonAudition3: String?
    let commonAudition4: String?
    let commonAudition5: String?
    let grade9Geapplicantsperseat5: String?
    let grade9Swdapplicantsperseat5: String?
    let directions2: String?
    let requirement62: String?
    let directions3: String?
    let directions4: String?
    let eligibility3: String?
    let eligibility4: String?
    let prgdesc6: String?
    let offerRate5: String?
    let offerRate6: String?
    let program6: String?
    let code6: String?
    let interest6: String?
    let method6: String?
    let seats9Ge6: String?
    let grade9Gefilledflag6: String?
    let grade9Geapplicants6: String?
    let seats9Swd6: String?
    let grade9Swdfilledflag6: String?
    let grade9Swdapplicants6: String?
    let seats106: String?
    let admissionspriority16: String?
    let admissionspriority25: String?
    let admissionspriority26: String?
    let admissionspriority33: String?
    let admissionspriority34: String?
    let admissionspriority35: String?
    let admissionspriority36: String?
    let admissionspriority46: String?
    let admissionspriority56: String?
    let grade9Geapplicantsperseat6: String?
    let grade9Swdapplicantsperseat6: String?
    let requirement61: String?
    let directions5: String?
    let directions6: String?
    let requirement16: String?
    let requirement26: String?
    let requirement36: String?
    let requirement46: String?
    let requirement56: String?
    let seats2Specialized: String?
    let seats3Specialized: String?
    let seats4Specialized: String?
    let seats5Specialized: String?
    let seats6Specialized: String?
    let applicants2Specialized: String?
    let applicants3Specialized: String?
    let applicants4Specialized: String?
    let applicants5Specialized: String?
    let applicants6Specialized: String?
    let appperseat2Specialized: String?
    let appperseat3Specialized: String?
    let appperseat4Specialized: String?
    let appperseat5Specialized: String?
    let appperseat6Specialized: String?
    let auditioninformation6: String?
    let commonAudition6: String?
    let boys: String?
    let admissionspriority52: String?
    let geoeligibility: String?
    let program7: String?
    let code7: String?
    let interest7: String?
    let method7: String?
    let seats9Ge7: String?
    let grade9Gefilledflag7: String?
    let grade9Geapplicants7: String?
    let seats9Swd7: String?
    let grade9Swdfilledflag7: String?
    let grade9Swdapplicants7: String?
    let seats107: String?
    let admissionspriority17: String?
    let admissionspriority44: String?
    let grade9Geapplicantsperseat7: String?
    let grade9Swdapplicantsperseat7: String?
    let admissionspriority54: String?
    let admissionspriority64: String?
    let admissionspriority74: String?
    let admissionspriority43: String?
    let admissionspriority53: String?
    let admissionspriority62: String?
    let admissionspriority63: String?
    let prgdesc7: String?
    let offerRate7: String?
    let admissionspriority27: String?
    let eligibility5: String?
    let requirement17: String?
    let requirement27: String?
    let requirement37: String?
    let requirement47: String?
    let eligibility6: String?
    let eligibility7: String?
    let auditioninformation7: String?
    let commonAudition7: String?
    let admissionspriority71: String?
    let program8: String?
    let code8: String?
    let interest8: String?
    let method8: String?
    let seats9Ge8: String?
    let grade9Gefilledflag8: String?
    let grade9Geapplicants8: String?
    let seats9Swd8: String?
    let grade9Swdfilledflag8: String?
    let grade9Swdapplicants8: String?
    let seats108: String?
    let admissionspriority18: String?
    let grade9Geapplicantsperseat8: String?
    let grade9Swdapplicantsperseat8: String?
    let directions7: String?
    let prgdesc8: String?
    let admissionspriority37: String?
    let prgdesc9: String?
    let requirement18: String?
    let offerRate8: String?
    let program9: String?
    let code9: String?
    let interest9: String?
    let method9: String?
    let seats9Ge9: String?
    let grade9Gefilledflag9: String?
    let grade9Geapplicants9: String?
    let seats9Swd9: String?
    let grade9Swdfilledflag9: String?
    let grade9Swdapplicants9: String?
    let seats109: String?
    let admissionspriority19: String?
    let admissionspriority28: String?
    let grade9Geapplicantsperseat9: String?
    let grade9Swdapplicantsperseat9: String?
    let requirement57: String?
    let requirement67: String?
    let prgdesc10: String?
    let requirement28: String?
    let requirement38: String?
    let offerRate9: String?
    let program10: String?
    let code10: String?
    let interest10: String?
    let method10: String?
    let seats9Ge10: String?
    let grade9Gefilledflag10: String?
    let grade9Geapplicants10: String?
    let seats9Swd10: String?
    let grade9Swdfilledflag10: String?
    let grade9Swdapplicants10: String?
    let seats1010: String?
    let admissionspriority110: String?
    let admissionspriority29: String?
    let grade9Geapplicantsperseat10: String?
    let grade9Swdapplicantsperseat10: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case boro = "boro"
        case overviewParagraph = "overview_paragraph"
        case academicopportunities1 = "academicopportunities1"
        case academicopportunities2 = "academicopportunities2"
        case academicopportunities3 = "academicopportunities3"
        case academicopportunities4 = "academicopportunities4"
        case academicopportunities5 = "academicopportunities5"
        case ellPrograms = "ell_programs"
        case languageClasses = "language_classes"
        case advancedplacementCourses = "advancedplacement_courses"
        case neighborhood = "neighborhood"
        case sharedSpace = "shared_space"
        case buildingCode = "building_code"
        case location = "location"
        case phoneNumber = "phone_number"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case website = "website"
        case subway = "subway"
        case bus = "bus"
        case grades2018 = "grades2018"
        case finalgrades = "finalgrades"
        case totalStudents = "total_students"
        case startTime = "start_time"
        case endTime = "end_time"
        case addtlInfo1 = "addtl_info1"
        case extracurricularActivities = "extracurricular_activities"
        case psalSportsBoys = "psal_sports_boys"
        case schoolSports = "school_sports"
        case graduationRate = "graduation_rate"
        case attendanceRate = "attendance_rate"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case collegeCareerRate = "college_career_rate"
        case pctStuSafe = "pct_stu_safe"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case offerRate1 = "offer_rate1"
        case program1 = "program1"
        case code1 = "code1"
        case interest1 = "interest1"
        case method1 = "method1"
        case seats9Ge1 = "seats9ge1"
        case grade9Gefilledflag1 = "grade9gefilledflag1"
        case grade9Geapplicants1 = "grade9geapplicants1"
        case seats9Swd1 = "seats9swd1"
        case grade9Swdfilledflag1 = "grade9swdfilledflag1"
        case grade9Swdapplicants1 = "grade9swdapplicants1"
        case seats101 = "seats101"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case admissionspriority41 = "admissionspriority41"
        case admissionspriority51 = "admissionspriority51"
        case grade9Geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9Swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case primaryAddressLine1 = "primary_address_line_1"
        case city = "city"
        case zip = "zip"
        case stateCode = "state_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case censusTract = "census_tract"
        case bin = "bin"
        case bbl = "bbl"
        case nta = "nta"
        case borough = "borough"
        case school10ThSeats = "school_10th_seats"
        case psalSportsGirls = "psal_sports_girls"
        case prgdesc1 = "prgdesc1"
        case prgdesc2 = "prgdesc2"
        case prgdesc3 = "prgdesc3"
        case requirement13 = "requirement1_3"
        case requirement23 = "requirement2_3"
        case requirement33 = "requirement3_3"
        case requirement43 = "requirement4_3"
        case requirement53 = "requirement5_3"
        case requirement63 = "requirement6_3"
        case program2 = "program2"
        case program3 = "program3"
        case code2 = "code2"
        case code3 = "code3"
        case interest2 = "interest2"
        case interest3 = "interest3"
        case method2 = "method2"
        case method3 = "method3"
        case seats9Ge2 = "seats9ge2"
        case seats9Ge3 = "seats9ge3"
        case grade9Gefilledflag2 = "grade9gefilledflag2"
        case grade9Gefilledflag3 = "grade9gefilledflag3"
        case grade9Geapplicants2 = "grade9geapplicants2"
        case grade9Geapplicants3 = "grade9geapplicants3"
        case seats9Swd2 = "seats9swd2"
        case seats9Swd3 = "seats9swd3"
        case grade9Swdfilledflag2 = "grade9swdfilledflag2"
        case grade9Swdfilledflag3 = "grade9swdfilledflag3"
        case grade9Swdapplicants2 = "grade9swdapplicants2"
        case grade9Swdapplicants3 = "grade9swdapplicants3"
        case seats102 = "seats102"
        case seats103 = "seats103"
        case admissionspriority13 = "admissionspriority13"
        case eligibility2 = "eligibility2"
        case grade9Geapplicantsperseat2 = "grade9geapplicantsperseat2"
        case grade9Geapplicantsperseat3 = "grade9geapplicantsperseat3"
        case grade9Swdapplicantsperseat2 = "grade9swdapplicantsperseat2"
        case grade9Swdapplicantsperseat3 = "grade9swdapplicantsperseat3"
        case diplomaendorsements = "diplomaendorsements"
        case pbat = "pbat"
        case requirement11 = "requirement1_1"
        case requirement21 = "requirement2_1"
        case requirement31 = "requirement3_1"
        case requirement41 = "requirement4_1"
        case requirement51 = "requirement5_1"
        case directions1 = "directions1"
        case earlycollege = "earlycollege"
        case psalSportsCoed = "psal_sports_coed"
        case campusName = "campus_name"
        case ptech = "ptech"
        case admissionspriority61 = "admissionspriority61"
        case girls = "girls"
        case offerRate2 = "offer_rate2"
        case admissionspriority12 = "admissionspriority12"
        case admissionspriority22 = "admissionspriority22"
        case admissionspriority32 = "admissionspriority32"
        case admissionspriority42 = "admissionspriority42"
        case eligibility1 = "eligibility1"
        case requirement12 = "requirement1_2"
        case auditioninformation1 = "auditioninformation1"
        case auditioninformation2 = "auditioninformation2"
        case commonAudition1 = "common_audition1"
        case commonAudition2 = "common_audition2"
        case requirement22 = "requirement2_2"
        case requirement32 = "requirement3_2"
        case prgdesc4 = "prgdesc4"
        case requirement14 = "requirement1_4"
        case requirement24 = "requirement2_4"
        case requirement34 = "requirement3_4"
        case requirement42 = "requirement4_2"
        case requirement44 = "requirement4_4"
        case offerRate3 = "offer_rate3"
        case offerRate4 = "offer_rate4"
        case program4 = "program4"
        case code4 = "code4"
        case interest4 = "interest4"
        case method4 = "method4"
        case seats9Ge4 = "seats9ge4"
        case grade9Gefilledflag4 = "grade9gefilledflag4"
        case grade9Geapplicants4 = "grade9geapplicants4"
        case seats9Swd4 = "seats9swd4"
        case grade9Swdfilledflag4 = "grade9swdfilledflag4"
        case grade9Swdapplicants4 = "grade9swdapplicants4"
        case seats104 = "seats104"
        case admissionspriority14 = "admissionspriority14"
        case admissionspriority23 = "admissionspriority23"
        case admissionspriority24 = "admissionspriority24"
        case auditioninformation3 = "auditioninformation3"
        case auditioninformation4 = "auditioninformation4"
        case grade9Geapplicantsperseat4 = "grade9geapplicantsperseat4"
        case grade9Swdapplicantsperseat4 = "grade9swdapplicantsperseat4"
        case transfer = "transfer"
        case international = "international"
        case specialized = "specialized"
        case seats1Specialized = "seats1specialized"
        case applicants1Specialized = "applicants1specialized"
        case appperseat1Specialized = "appperseat1specialized"
        case prgdesc5 = "prgdesc5"
        case requirement15 = "requirement1_5"
        case requirement25 = "requirement2_5"
        case requirement35 = "requirement3_5"
        case requirement45 = "requirement4_5"
        case requirement52 = "requirement5_2"
        case requirement54 = "requirement5_4"
        case requirement55 = "requirement5_5"
        case program5 = "program5"
        case code5 = "code5"
        case interest5 = "interest5"
        case method5 = "method5"
        case seats9Ge5 = "seats9ge5"
        case grade9Gefilledflag5 = "grade9gefilledflag5"
        case grade9Geapplicants5 = "grade9geapplicants5"
        case seats9Swd5 = "seats9swd5"
        case grade9Swdfilledflag5 = "grade9swdfilledflag5"
        case grade9Swdapplicants5 = "grade9swdapplicants5"
        case seats105 = "seats105"
        case admissionspriority15 = "admissionspriority15"
        case auditioninformation5 = "auditioninformation5"
        case commonAudition3 = "common_audition3"
        case commonAudition4 = "common_audition4"
        case commonAudition5 = "common_audition5"
        case grade9Geapplicantsperseat5 = "grade9geapplicantsperseat5"
        case grade9Swdapplicantsperseat5 = "grade9swdapplicantsperseat5"
        case directions2 = "directions2"
        case requirement62 = "requirement6_2"
        case directions3 = "directions3"
        case directions4 = "directions4"
        case eligibility3 = "eligibility3"
        case eligibility4 = "eligibility4"
        case prgdesc6 = "prgdesc6"
        case offerRate5 = "offer_rate5"
        case offerRate6 = "offer_rate6"
        case program6 = "program6"
        case code6 = "code6"
        case interest6 = "interest6"
        case method6 = "method6"
        case seats9Ge6 = "seats9ge6"
        case grade9Gefilledflag6 = "grade9gefilledflag6"
        case grade9Geapplicants6 = "grade9geapplicants6"
        case seats9Swd6 = "seats9swd6"
        case grade9Swdfilledflag6 = "grade9swdfilledflag6"
        case grade9Swdapplicants6 = "grade9swdapplicants6"
        case seats106 = "seats106"
        case admissionspriority16 = "admissionspriority16"
        case admissionspriority25 = "admissionspriority25"
        case admissionspriority26 = "admissionspriority26"
        case admissionspriority33 = "admissionspriority33"
        case admissionspriority34 = "admissionspriority34"
        case admissionspriority35 = "admissionspriority35"
        case admissionspriority36 = "admissionspriority36"
        case admissionspriority46 = "admissionspriority46"
        case admissionspriority56 = "admissionspriority56"
        case grade9Geapplicantsperseat6 = "grade9geapplicantsperseat6"
        case grade9Swdapplicantsperseat6 = "grade9swdapplicantsperseat6"
        case requirement61 = "requirement6_1"
        case directions5 = "directions5"
        case directions6 = "directions6"
        case requirement16 = "requirement1_6"
        case requirement26 = "requirement2_6"
        case requirement36 = "requirement3_6"
        case requirement46 = "requirement4_6"
        case requirement56 = "requirement5_6"
        case seats2Specialized = "seats2specialized"
        case seats3Specialized = "seats3specialized"
        case seats4Specialized = "seats4specialized"
        case seats5Specialized = "seats5specialized"
        case seats6Specialized = "seats6specialized"
        case applicants2Specialized = "applicants2specialized"
        case applicants3Specialized = "applicants3specialized"
        case applicants4Specialized = "applicants4specialized"
        case applicants5Specialized = "applicants5specialized"
        case applicants6Specialized = "applicants6specialized"
        case appperseat2Specialized = "appperseat2specialized"
        case appperseat3Specialized = "appperseat3specialized"
        case appperseat4Specialized = "appperseat4specialized"
        case appperseat5Specialized = "appperseat5specialized"
        case appperseat6Specialized = "appperseat6specialized"
        case auditioninformation6 = "auditioninformation6"
        case commonAudition6 = "common_audition6"
        case boys = "boys"
        case admissionspriority52 = "admissionspriority52"
        case geoeligibility = "geoeligibility"
        case program7 = "program7"
        case code7 = "code7"
        case interest7 = "interest7"
        case method7 = "method7"
        case seats9Ge7 = "seats9ge7"
        case grade9Gefilledflag7 = "grade9gefilledflag7"
        case grade9Geapplicants7 = "grade9geapplicants7"
        case seats9Swd7 = "seats9swd7"
        case grade9Swdfilledflag7 = "grade9swdfilledflag7"
        case grade9Swdapplicants7 = "grade9swdapplicants7"
        case seats107 = "seats107"
        case admissionspriority17 = "admissionspriority17"
        case admissionspriority44 = "admissionspriority44"
        case grade9Geapplicantsperseat7 = "grade9geapplicantsperseat7"
        case grade9Swdapplicantsperseat7 = "grade9swdapplicantsperseat7"
        case admissionspriority54 = "admissionspriority54"
        case admissionspriority64 = "admissionspriority64"
        case admissionspriority74 = "admissionspriority74"
        case admissionspriority43 = "admissionspriority43"
        case admissionspriority53 = "admissionspriority53"
        case admissionspriority62 = "admissionspriority62"
        case admissionspriority63 = "admissionspriority63"
        case prgdesc7 = "prgdesc7"
        case offerRate7 = "offer_rate7"
        case admissionspriority27 = "admissionspriority27"
        case eligibility5 = "eligibility5"
        case requirement17 = "requirement1_7"
        case requirement27 = "requirement2_7"
        case requirement37 = "requirement3_7"
        case requirement47 = "requirement4_7"
        case eligibility6 = "eligibility6"
        case eligibility7 = "eligibility7"
        case auditioninformation7 = "auditioninformation7"
        case commonAudition7 = "common_audition7"
        case admissionspriority71 = "admissionspriority71"
        case program8 = "program8"
        case code8 = "code8"
        case interest8 = "interest8"
        case method8 = "method8"
        case seats9Ge8 = "seats9ge8"
        case grade9Gefilledflag8 = "grade9gefilledflag8"
        case grade9Geapplicants8 = "grade9geapplicants8"
        case seats9Swd8 = "seats9swd8"
        case grade9Swdfilledflag8 = "grade9swdfilledflag8"
        case grade9Swdapplicants8 = "grade9swdapplicants8"
        case seats108 = "seats108"
        case admissionspriority18 = "admissionspriority18"
        case grade9Geapplicantsperseat8 = "grade9geapplicantsperseat8"
        case grade9Swdapplicantsperseat8 = "grade9swdapplicantsperseat8"
        case directions7 = "directions7"
        case prgdesc8 = "prgdesc8"
        case admissionspriority37 = "admissionspriority37"
        case prgdesc9 = "prgdesc9"
        case requirement18 = "requirement1_8"
        case offerRate8 = "offer_rate8"
        case program9 = "program9"
        case code9 = "code9"
        case interest9 = "interest9"
        case method9 = "method9"
        case seats9Ge9 = "seats9ge9"
        case grade9Gefilledflag9 = "grade9gefilledflag9"
        case grade9Geapplicants9 = "grade9geapplicants9"
        case seats9Swd9 = "seats9swd9"
        case grade9Swdfilledflag9 = "grade9swdfilledflag9"
        case grade9Swdapplicants9 = "grade9swdapplicants9"
        case seats109 = "seats109"
        case admissionspriority19 = "admissionspriority19"
        case admissionspriority28 = "admissionspriority28"
        case grade9Geapplicantsperseat9 = "grade9geapplicantsperseat9"
        case grade9Swdapplicantsperseat9 = "grade9swdapplicantsperseat9"
        case requirement57 = "requirement5_7"
        case requirement67 = "requirement6_7"
        case prgdesc10 = "prgdesc10"
        case requirement28 = "requirement2_8"
        case requirement38 = "requirement3_8"
        case offerRate9 = "offer_rate9"
        case program10 = "program10"
        case code10 = "code10"
        case interest10 = "interest10"
        case method10 = "method10"
        case seats9Ge10 = "seats9ge10"
        case grade9Gefilledflag10 = "grade9gefilledflag10"
        case grade9Geapplicants10 = "grade9geapplicants10"
        case seats9Swd10 = "seats9swd10"
        case grade9Swdfilledflag10 = "grade9swdfilledflag10"
        case grade9Swdapplicants10 = "grade9swdapplicants10"
        case seats1010 = "seats1010"
        case admissionspriority110 = "admissionspriority110"
        case admissionspriority29 = "admissionspriority29"
        case grade9Geapplicantsperseat10 = "grade9geapplicantsperseat10"
        case grade9Swdapplicantsperseat10 = "grade9swdapplicantsperseat10"
    }
    
    init(dbn: String, schoolName: String, boro: String, overviewParagraph: String, academicopportunities1: String?, academicopportunities2: String?, academicopportunities3: String?, academicopportunities4: String?, academicopportunities5: String?, ellPrograms: String, languageClasses: String?, advancedplacementCourses: String?, neighborhood: String, sharedSpace: String?, buildingCode: String?, location: String, phoneNumber: String, faxNumber: String?, schoolEmail: String?, website: String, subway: String, bus: String, grades2018: String, finalgrades: String, totalStudents: String, startTime: String?, endTime: String?, addtlInfo1: String?, extracurricularActivities: String?, psalSportsBoys: String?, schoolSports: String?, graduationRate: String?, attendanceRate: String, pctStuEnoughVariety: String?, collegeCareerRate: String?, pctStuSafe: String?, schoolAccessibilityDescription: String?, offerRate1: String?, program1: String, code1: String, interest1: String, method1: String, seats9Ge1: String?, grade9Gefilledflag1: String?, grade9Geapplicants1: String?, seats9Swd1: String?, grade9Swdfilledflag1: String?, grade9Swdapplicants1: String?, seats101: String, admissionspriority11: String?, admissionspriority21: String?, admissionspriority31: String?, admissionspriority41: String?, admissionspriority51: String?, grade9Geapplicantsperseat1: String?, grade9Swdapplicantsperseat1: String?, primaryAddressLine1: String, city: String, zip: String, stateCode: String, latitude: String?, longitude: String?, communityBoard: String?, councilDistrict: String?, censusTract: String?, bin: String?, bbl: String?, nta: String?, borough: String?, school10ThSeats: String?, psalSportsGirls: String?, prgdesc1: String?, prgdesc2: String?, prgdesc3: String?, requirement13: String?, requirement23: String?, requirement33: String?, requirement43: String?, requirement53: String?, requirement63: String?, program2: String?, program3: String?, code2: String?, code3: String?, interest2: String?, interest3: String?, method2: String?, method3: String?, seats9Ge2: String?, seats9Ge3: String?, grade9Gefilledflag2: String?, grade9Gefilledflag3: String?, grade9Geapplicants2: String?, grade9Geapplicants3: String?, seats9Swd2: String?, seats9Swd3: String?, grade9Swdfilledflag2: String?, grade9Swdfilledflag3: String?, grade9Swdapplicants2: String?, grade9Swdapplicants3: String?, seats102: String?, seats103: String?, admissionspriority13: String?, eligibility2: String?, grade9Geapplicantsperseat2: String?, grade9Geapplicantsperseat3: String?, grade9Swdapplicantsperseat2: String?, grade9Swdapplicantsperseat3: String?, diplomaendorsements: String?, pbat: String?, requirement11: String?, requirement21: String?, requirement31: String?, requirement41: String?, requirement51: String?, directions1: String?, earlycollege: String?, psalSportsCoed: String?, campusName: String?, ptech: String?, admissionspriority61: String?, girls: String?, offerRate2: String?, admissionspriority12: String?, admissionspriority22: String?, admissionspriority32: String?, admissionspriority42: String?, eligibility1: String?, requirement12: String?, auditioninformation1: String?, auditioninformation2: String?, commonAudition1: String?, commonAudition2: String?, requirement22: String?, requirement32: String?, prgdesc4: String?, requirement14: String?, requirement24: String?, requirement34: String?, requirement42: String?, requirement44: String?, offerRate3: String?, offerRate4: String?, program4: String?, code4: String?, interest4: String?, method4: String?, seats9Ge4: String?, grade9Gefilledflag4: String?, grade9Geapplicants4: String?, seats9Swd4: String?, grade9Swdfilledflag4: String?, grade9Swdapplicants4: String?, seats104: String?, admissionspriority14: String?, admissionspriority23: String?, admissionspriority24: String?, auditioninformation3: String?, auditioninformation4: String?, grade9Geapplicantsperseat4: String?, grade9Swdapplicantsperseat4: String?, transfer: String?, international: String?, specialized: String?, seats1Specialized: String?, applicants1Specialized: String?, appperseat1Specialized: String?, prgdesc5: String?, requirement15: String?, requirement25: String?, requirement35: String?, requirement45: String?, requirement52: String?, requirement54: String?, requirement55: String?, program5: String?, code5: String?, interest5: String?, method5: String?, seats9Ge5: String?, grade9Gefilledflag5: String?, grade9Geapplicants5: String?, seats9Swd5: String?, grade9Swdfilledflag5: String?, grade9Swdapplicants5: String?, seats105: String?, admissionspriority15: String?, auditioninformation5: String?, commonAudition3: String?, commonAudition4: String?, commonAudition5: String?, grade9Geapplicantsperseat5: String?, grade9Swdapplicantsperseat5: String?, directions2: String?, requirement62: String?, directions3: String?, directions4: String?, eligibility3: String?, eligibility4: String?, prgdesc6: String?, offerRate5: String?, offerRate6: String?, program6: String?, code6: String?, interest6: String?, method6: String?, seats9Ge6: String?, grade9Gefilledflag6: String?, grade9Geapplicants6: String?, seats9Swd6: String?, grade9Swdfilledflag6: String?, grade9Swdapplicants6: String?, seats106: String?, admissionspriority16: String?, admissionspriority25: String?, admissionspriority26: String?, admissionspriority33: String?, admissionspriority34: String?, admissionspriority35: String?, admissionspriority36: String?, admissionspriority46: String?, admissionspriority56: String?, grade9Geapplicantsperseat6: String?, grade9Swdapplicantsperseat6: String?, requirement61: String?, directions5: String?, directions6: String?, requirement16: String?, requirement26: String?, requirement36: String?, requirement46: String?, requirement56: String?, seats2Specialized: String?, seats3Specialized: String?, seats4Specialized: String?, seats5Specialized: String?, seats6Specialized: String?, applicants2Specialized: String?, applicants3Specialized: String?, applicants4Specialized: String?, applicants5Specialized: String?, applicants6Specialized: String?, appperseat2Specialized: String?, appperseat3Specialized: String?, appperseat4Specialized: String?, appperseat5Specialized: String?, appperseat6Specialized: String?, auditioninformation6: String?, commonAudition6: String?, boys: String?, admissionspriority52: String?, geoeligibility: String?, program7: String?, code7: String?, interest7: String?, method7: String?, seats9Ge7: String?, grade9Gefilledflag7: String?, grade9Geapplicants7: String?, seats9Swd7: String?, grade9Swdfilledflag7: String?, grade9Swdapplicants7: String?, seats107: String?, admissionspriority17: String?, admissionspriority44: String?, grade9Geapplicantsperseat7: String?, grade9Swdapplicantsperseat7: String?, admissionspriority54: String?, admissionspriority64: String?, admissionspriority74: String?, admissionspriority43: String?, admissionspriority53: String?, admissionspriority62: String?, admissionspriority63: String?, prgdesc7: String?, offerRate7: String?, admissionspriority27: String?, eligibility5: String?, requirement17: String?, requirement27: String?, requirement37: String?, requirement47: String?, eligibility6: String?, eligibility7: String?, auditioninformation7: String?, commonAudition7: String?, admissionspriority71: String?, program8: String?, code8: String?, interest8: String?, method8: String?, seats9Ge8: String?, grade9Gefilledflag8: String?, grade9Geapplicants8: String?, seats9Swd8: String?, grade9Swdfilledflag8: String?, grade9Swdapplicants8: String?, seats108: String?, admissionspriority18: String?, grade9Geapplicantsperseat8: String?, grade9Swdapplicantsperseat8: String?, directions7: String?, prgdesc8: String?, admissionspriority37: String?, prgdesc9: String?, requirement18: String?, offerRate8: String?, program9: String?, code9: String?, interest9: String?, method9: String?, seats9Ge9: String?, grade9Gefilledflag9: String?, grade9Geapplicants9: String?, seats9Swd9: String?, grade9Swdfilledflag9: String?, grade9Swdapplicants9: String?, seats109: String?, admissionspriority19: String?, admissionspriority28: String?, grade9Geapplicantsperseat9: String?, grade9Swdapplicantsperseat9: String?, requirement57: String?, requirement67: String?, prgdesc10: String?, requirement28: String?, requirement38: String?, offerRate9: String?, program10: String?, code10: String?, interest10: String?, method10: String?, seats9Ge10: String?, grade9Gefilledflag10: String?, grade9Geapplicants10: String?, seats9Swd10: String?, grade9Swdfilledflag10: String?, grade9Swdapplicants10: String?, seats1010: String?, admissionspriority110: String?, admissionspriority29: String?, grade9Geapplicantsperseat10: String?, grade9Swdapplicantsperseat10: String?) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.boro = boro
        self.overviewParagraph = overviewParagraph
        self.academicopportunities1 = academicopportunities1
        self.academicopportunities2 = academicopportunities2
        self.academicopportunities3 = academicopportunities3
        self.academicopportunities4 = academicopportunities4
        self.academicopportunities5 = academicopportunities5
        self.ellPrograms = ellPrograms
        self.languageClasses = languageClasses
        self.advancedplacementCourses = advancedplacementCourses
        self.neighborhood = neighborhood
        self.sharedSpace = sharedSpace
        self.buildingCode = buildingCode
        self.location = location
        self.phoneNumber = phoneNumber
        self.faxNumber = faxNumber
        self.schoolEmail = schoolEmail
        self.website = website
        self.subway = subway
        self.bus = bus
        self.grades2018 = grades2018
        self.finalgrades = finalgrades
        self.totalStudents = totalStudents
        self.startTime = startTime
        self.endTime = endTime
        self.addtlInfo1 = addtlInfo1
        self.extracurricularActivities = extracurricularActivities
        self.psalSportsBoys = psalSportsBoys
        self.schoolSports = schoolSports
        self.graduationRate = graduationRate
        self.attendanceRate = attendanceRate
        self.pctStuEnoughVariety = pctStuEnoughVariety
        self.collegeCareerRate = collegeCareerRate
        self.pctStuSafe = pctStuSafe
        self.schoolAccessibilityDescription = schoolAccessibilityDescription
        self.offerRate1 = offerRate1
        self.program1 = program1
        self.code1 = code1
        self.interest1 = interest1
        self.method1 = method1
        self.seats9Ge1 = seats9Ge1
        self.grade9Gefilledflag1 = grade9Gefilledflag1
        self.grade9Geapplicants1 = grade9Geapplicants1
        self.seats9Swd1 = seats9Swd1
        self.grade9Swdfilledflag1 = grade9Swdfilledflag1
        self.grade9Swdapplicants1 = grade9Swdapplicants1
        self.seats101 = seats101
        self.admissionspriority11 = admissionspriority11
        self.admissionspriority21 = admissionspriority21
        self.admissionspriority31 = admissionspriority31
        self.admissionspriority41 = admissionspriority41
        self.admissionspriority51 = admissionspriority51
        self.grade9Geapplicantsperseat1 = grade9Geapplicantsperseat1
        self.grade9Swdapplicantsperseat1 = grade9Swdapplicantsperseat1
        self.primaryAddressLine1 = primaryAddressLine1
        self.city = city
        self.zip = zip
        self.stateCode = stateCode
        self.latitude = latitude
        self.longitude = longitude
        self.communityBoard = communityBoard
        self.councilDistrict = councilDistrict
        self.censusTract = censusTract
        self.bin = bin
        self.bbl = bbl
        self.nta = nta
        self.borough = borough
        self.school10ThSeats = school10ThSeats
        self.psalSportsGirls = psalSportsGirls
        self.prgdesc1 = prgdesc1
        self.prgdesc2 = prgdesc2
        self.prgdesc3 = prgdesc3
        self.requirement13 = requirement13
        self.requirement23 = requirement23
        self.requirement33 = requirement33
        self.requirement43 = requirement43
        self.requirement53 = requirement53
        self.requirement63 = requirement63
        self.program2 = program2
        self.program3 = program3
        self.code2 = code2
        self.code3 = code3
        self.interest2 = interest2
        self.interest3 = interest3
        self.method2 = method2
        self.method3 = method3
        self.seats9Ge2 = seats9Ge2
        self.seats9Ge3 = seats9Ge3
        self.grade9Gefilledflag2 = grade9Gefilledflag2
        self.grade9Gefilledflag3 = grade9Gefilledflag3
        self.grade9Geapplicants2 = grade9Geapplicants2
        self.grade9Geapplicants3 = grade9Geapplicants3
        self.seats9Swd2 = seats9Swd2
        self.seats9Swd3 = seats9Swd3
        self.grade9Swdfilledflag2 = grade9Swdfilledflag2
        self.grade9Swdfilledflag3 = grade9Swdfilledflag3
        self.grade9Swdapplicants2 = grade9Swdapplicants2
        self.grade9Swdapplicants3 = grade9Swdapplicants3
        self.seats102 = seats102
        self.seats103 = seats103
        self.admissionspriority13 = admissionspriority13
        self.eligibility2 = eligibility2
        self.grade9Geapplicantsperseat2 = grade9Geapplicantsperseat2
        self.grade9Geapplicantsperseat3 = grade9Geapplicantsperseat3
        self.grade9Swdapplicantsperseat2 = grade9Swdapplicantsperseat2
        self.grade9Swdapplicantsperseat3 = grade9Swdapplicantsperseat3
        self.diplomaendorsements = diplomaendorsements
        self.pbat = pbat
        self.requirement11 = requirement11
        self.requirement21 = requirement21
        self.requirement31 = requirement31
        self.requirement41 = requirement41
        self.requirement51 = requirement51
        self.directions1 = directions1
        self.earlycollege = earlycollege
        self.psalSportsCoed = psalSportsCoed
        self.campusName = campusName
        self.ptech = ptech
        self.admissionspriority61 = admissionspriority61
        self.girls = girls
        self.offerRate2 = offerRate2
        self.admissionspriority12 = admissionspriority12
        self.admissionspriority22 = admissionspriority22
        self.admissionspriority32 = admissionspriority32
        self.admissionspriority42 = admissionspriority42
        self.eligibility1 = eligibility1
        self.requirement12 = requirement12
        self.auditioninformation1 = auditioninformation1
        self.auditioninformation2 = auditioninformation2
        self.commonAudition1 = commonAudition1
        self.commonAudition2 = commonAudition2
        self.requirement22 = requirement22
        self.requirement32 = requirement32
        self.prgdesc4 = prgdesc4
        self.requirement14 = requirement14
        self.requirement24 = requirement24
        self.requirement34 = requirement34
        self.requirement42 = requirement42
        self.requirement44 = requirement44
        self.offerRate3 = offerRate3
        self.offerRate4 = offerRate4
        self.program4 = program4
        self.code4 = code4
        self.interest4 = interest4
        self.method4 = method4
        self.seats9Ge4 = seats9Ge4
        self.grade9Gefilledflag4 = grade9Gefilledflag4
        self.grade9Geapplicants4 = grade9Geapplicants4
        self.seats9Swd4 = seats9Swd4
        self.grade9Swdfilledflag4 = grade9Swdfilledflag4
        self.grade9Swdapplicants4 = grade9Swdapplicants4
        self.seats104 = seats104
        self.admissionspriority14 = admissionspriority14
        self.admissionspriority23 = admissionspriority23
        self.admissionspriority24 = admissionspriority24
        self.auditioninformation3 = auditioninformation3
        self.auditioninformation4 = auditioninformation4
        self.grade9Geapplicantsperseat4 = grade9Geapplicantsperseat4
        self.grade9Swdapplicantsperseat4 = grade9Swdapplicantsperseat4
        self.transfer = transfer
        self.international = international
        self.specialized = specialized
        self.seats1Specialized = seats1Specialized
        self.applicants1Specialized = applicants1Specialized
        self.appperseat1Specialized = appperseat1Specialized
        self.prgdesc5 = prgdesc5
        self.requirement15 = requirement15
        self.requirement25 = requirement25
        self.requirement35 = requirement35
        self.requirement45 = requirement45
        self.requirement52 = requirement52
        self.requirement54 = requirement54
        self.requirement55 = requirement55
        self.program5 = program5
        self.code5 = code5
        self.interest5 = interest5
        self.method5 = method5
        self.seats9Ge5 = seats9Ge5
        self.grade9Gefilledflag5 = grade9Gefilledflag5
        self.grade9Geapplicants5 = grade9Geapplicants5
        self.seats9Swd5 = seats9Swd5
        self.grade9Swdfilledflag5 = grade9Swdfilledflag5
        self.grade9Swdapplicants5 = grade9Swdapplicants5
        self.seats105 = seats105
        self.admissionspriority15 = admissionspriority15
        self.auditioninformation5 = auditioninformation5
        self.commonAudition3 = commonAudition3
        self.commonAudition4 = commonAudition4
        self.commonAudition5 = commonAudition5
        self.grade9Geapplicantsperseat5 = grade9Geapplicantsperseat5
        self.grade9Swdapplicantsperseat5 = grade9Swdapplicantsperseat5
        self.directions2 = directions2
        self.requirement62 = requirement62
        self.directions3 = directions3
        self.directions4 = directions4
        self.eligibility3 = eligibility3
        self.eligibility4 = eligibility4
        self.prgdesc6 = prgdesc6
        self.offerRate5 = offerRate5
        self.offerRate6 = offerRate6
        self.program6 = program6
        self.code6 = code6
        self.interest6 = interest6
        self.method6 = method6
        self.seats9Ge6 = seats9Ge6
        self.grade9Gefilledflag6 = grade9Gefilledflag6
        self.grade9Geapplicants6 = grade9Geapplicants6
        self.seats9Swd6 = seats9Swd6
        self.grade9Swdfilledflag6 = grade9Swdfilledflag6
        self.grade9Swdapplicants6 = grade9Swdapplicants6
        self.seats106 = seats106
        self.admissionspriority16 = admissionspriority16
        self.admissionspriority25 = admissionspriority25
        self.admissionspriority26 = admissionspriority26
        self.admissionspriority33 = admissionspriority33
        self.admissionspriority34 = admissionspriority34
        self.admissionspriority35 = admissionspriority35
        self.admissionspriority36 = admissionspriority36
        self.admissionspriority46 = admissionspriority46
        self.admissionspriority56 = admissionspriority56
        self.grade9Geapplicantsperseat6 = grade9Geapplicantsperseat6
        self.grade9Swdapplicantsperseat6 = grade9Swdapplicantsperseat6
        self.requirement61 = requirement61
        self.directions5 = directions5
        self.directions6 = directions6
        self.requirement16 = requirement16
        self.requirement26 = requirement26
        self.requirement36 = requirement36
        self.requirement46 = requirement46
        self.requirement56 = requirement56
        self.seats2Specialized = seats2Specialized
        self.seats3Specialized = seats3Specialized
        self.seats4Specialized = seats4Specialized
        self.seats5Specialized = seats5Specialized
        self.seats6Specialized = seats6Specialized
        self.applicants2Specialized = applicants2Specialized
        self.applicants3Specialized = applicants3Specialized
        self.applicants4Specialized = applicants4Specialized
        self.applicants5Specialized = applicants5Specialized
        self.applicants6Specialized = applicants6Specialized
        self.appperseat2Specialized = appperseat2Specialized
        self.appperseat3Specialized = appperseat3Specialized
        self.appperseat4Specialized = appperseat4Specialized
        self.appperseat5Specialized = appperseat5Specialized
        self.appperseat6Specialized = appperseat6Specialized
        self.auditioninformation6 = auditioninformation6
        self.commonAudition6 = commonAudition6
        self.boys = boys
        self.admissionspriority52 = admissionspriority52
        self.geoeligibility = geoeligibility
        self.program7 = program7
        self.code7 = code7
        self.interest7 = interest7
        self.method7 = method7
        self.seats9Ge7 = seats9Ge7
        self.grade9Gefilledflag7 = grade9Gefilledflag7
        self.grade9Geapplicants7 = grade9Geapplicants7
        self.seats9Swd7 = seats9Swd7
        self.grade9Swdfilledflag7 = grade9Swdfilledflag7
        self.grade9Swdapplicants7 = grade9Swdapplicants7
        self.seats107 = seats107
        self.admissionspriority17 = admissionspriority17
        self.admissionspriority44 = admissionspriority44
        self.grade9Geapplicantsperseat7 = grade9Geapplicantsperseat7
        self.grade9Swdapplicantsperseat7 = grade9Swdapplicantsperseat7
        self.admissionspriority54 = admissionspriority54
        self.admissionspriority64 = admissionspriority64
        self.admissionspriority74 = admissionspriority74
        self.admissionspriority43 = admissionspriority43
        self.admissionspriority53 = admissionspriority53
        self.admissionspriority62 = admissionspriority62
        self.admissionspriority63 = admissionspriority63
        self.prgdesc7 = prgdesc7
        self.offerRate7 = offerRate7
        self.admissionspriority27 = admissionspriority27
        self.eligibility5 = eligibility5
        self.requirement17 = requirement17
        self.requirement27 = requirement27
        self.requirement37 = requirement37
        self.requirement47 = requirement47
        self.eligibility6 = eligibility6
        self.eligibility7 = eligibility7
        self.auditioninformation7 = auditioninformation7
        self.commonAudition7 = commonAudition7
        self.admissionspriority71 = admissionspriority71
        self.program8 = program8
        self.code8 = code8
        self.interest8 = interest8
        self.method8 = method8
        self.seats9Ge8 = seats9Ge8
        self.grade9Gefilledflag8 = grade9Gefilledflag8
        self.grade9Geapplicants8 = grade9Geapplicants8
        self.seats9Swd8 = seats9Swd8
        self.grade9Swdfilledflag8 = grade9Swdfilledflag8
        self.grade9Swdapplicants8 = grade9Swdapplicants8
        self.seats108 = seats108
        self.admissionspriority18 = admissionspriority18
        self.grade9Geapplicantsperseat8 = grade9Geapplicantsperseat8
        self.grade9Swdapplicantsperseat8 = grade9Swdapplicantsperseat8
        self.directions7 = directions7
        self.prgdesc8 = prgdesc8
        self.admissionspriority37 = admissionspriority37
        self.prgdesc9 = prgdesc9
        self.requirement18 = requirement18
        self.offerRate8 = offerRate8
        self.program9 = program9
        self.code9 = code9
        self.interest9 = interest9
        self.method9 = method9
        self.seats9Ge9 = seats9Ge9
        self.grade9Gefilledflag9 = grade9Gefilledflag9
        self.grade9Geapplicants9 = grade9Geapplicants9
        self.seats9Swd9 = seats9Swd9
        self.grade9Swdfilledflag9 = grade9Swdfilledflag9
        self.grade9Swdapplicants9 = grade9Swdapplicants9
        self.seats109 = seats109
        self.admissionspriority19 = admissionspriority19
        self.admissionspriority28 = admissionspriority28
        self.grade9Geapplicantsperseat9 = grade9Geapplicantsperseat9
        self.grade9Swdapplicantsperseat9 = grade9Swdapplicantsperseat9
        self.requirement57 = requirement57
        self.requirement67 = requirement67
        self.prgdesc10 = prgdesc10
        self.requirement28 = requirement28
        self.requirement38 = requirement38
        self.offerRate9 = offerRate9
        self.program10 = program10
        self.code10 = code10
        self.interest10 = interest10
        self.method10 = method10
        self.seats9Ge10 = seats9Ge10
        self.grade9Gefilledflag10 = grade9Gefilledflag10
        self.grade9Geapplicants10 = grade9Geapplicants10
        self.seats9Swd10 = seats9Swd10
        self.grade9Swdfilledflag10 = grade9Swdfilledflag10
        self.grade9Swdapplicants10 = grade9Swdapplicants10
        self.seats1010 = seats1010
        self.admissionspriority110 = admissionspriority110
        self.admissionspriority29 = admissionspriority29
        self.grade9Geapplicantsperseat10 = grade9Geapplicantsperseat10
        self.grade9Swdapplicantsperseat10 = grade9Swdapplicantsperseat10
    }
}

// MARK: SchoolElement convenience initializers and mutators

extension SchoolElement {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(SchoolElement.self, from: data)
        self.init(dbn: me.dbn, schoolName: me.schoolName, boro: me.boro, overviewParagraph: me.overviewParagraph, academicopportunities1: me.academicopportunities1, academicopportunities2: me.academicopportunities2, academicopportunities3: me.academicopportunities3, academicopportunities4: me.academicopportunities4, academicopportunities5: me.academicopportunities5, ellPrograms: me.ellPrograms, languageClasses: me.languageClasses, advancedplacementCourses: me.advancedplacementCourses, neighborhood: me.neighborhood, sharedSpace: me.sharedSpace, buildingCode: me.buildingCode, location: me.location, phoneNumber: me.phoneNumber, faxNumber: me.faxNumber, schoolEmail: me.schoolEmail, website: me.website, subway: me.subway, bus: me.bus, grades2018: me.grades2018, finalgrades: me.finalgrades, totalStudents: me.totalStudents, startTime: me.startTime, endTime: me.endTime, addtlInfo1: me.addtlInfo1, extracurricularActivities: me.extracurricularActivities, psalSportsBoys: me.psalSportsBoys, schoolSports: me.schoolSports, graduationRate: me.graduationRate, attendanceRate: me.attendanceRate, pctStuEnoughVariety: me.pctStuEnoughVariety, collegeCareerRate: me.collegeCareerRate, pctStuSafe: me.pctStuSafe, schoolAccessibilityDescription: me.schoolAccessibilityDescription, offerRate1: me.offerRate1, program1: me.program1, code1: me.code1, interest1: me.interest1, method1: me.method1, seats9Ge1: me.seats9Ge1, grade9Gefilledflag1: me.grade9Gefilledflag1, grade9Geapplicants1: me.grade9Geapplicants1, seats9Swd1: me.seats9Swd1, grade9Swdfilledflag1: me.grade9Swdfilledflag1, grade9Swdapplicants1: me.grade9Swdapplicants1, seats101: me.seats101, admissionspriority11: me.admissionspriority11, admissionspriority21: me.admissionspriority21, admissionspriority31: me.admissionspriority31, admissionspriority41: me.admissionspriority41, admissionspriority51: me.admissionspriority51, grade9Geapplicantsperseat1: me.grade9Geapplicantsperseat1, grade9Swdapplicantsperseat1: me.grade9Swdapplicantsperseat1, primaryAddressLine1: me.primaryAddressLine1, city: me.city, zip: me.zip, stateCode: me.stateCode, latitude: me.latitude, longitude: me.longitude, communityBoard: me.communityBoard, councilDistrict: me.councilDistrict, censusTract: me.censusTract, bin: me.bin, bbl: me.bbl, nta: me.nta, borough: me.borough, school10ThSeats: me.school10ThSeats, psalSportsGirls: me.psalSportsGirls, prgdesc1: me.prgdesc1, prgdesc2: me.prgdesc2, prgdesc3: me.prgdesc3, requirement13: me.requirement13, requirement23: me.requirement23, requirement33: me.requirement33, requirement43: me.requirement43, requirement53: me.requirement53, requirement63: me.requirement63, program2: me.program2, program3: me.program3, code2: me.code2, code3: me.code3, interest2: me.interest2, interest3: me.interest3, method2: me.method2, method3: me.method3, seats9Ge2: me.seats9Ge2, seats9Ge3: me.seats9Ge3, grade9Gefilledflag2: me.grade9Gefilledflag2, grade9Gefilledflag3: me.grade9Gefilledflag3, grade9Geapplicants2: me.grade9Geapplicants2, grade9Geapplicants3: me.grade9Geapplicants3, seats9Swd2: me.seats9Swd2, seats9Swd3: me.seats9Swd3, grade9Swdfilledflag2: me.grade9Swdfilledflag2, grade9Swdfilledflag3: me.grade9Swdfilledflag3, grade9Swdapplicants2: me.grade9Swdapplicants2, grade9Swdapplicants3: me.grade9Swdapplicants3, seats102: me.seats102, seats103: me.seats103, admissionspriority13: me.admissionspriority13, eligibility2: me.eligibility2, grade9Geapplicantsperseat2: me.grade9Geapplicantsperseat2, grade9Geapplicantsperseat3: me.grade9Geapplicantsperseat3, grade9Swdapplicantsperseat2: me.grade9Swdapplicantsperseat2, grade9Swdapplicantsperseat3: me.grade9Swdapplicantsperseat3, diplomaendorsements: me.diplomaendorsements, pbat: me.pbat, requirement11: me.requirement11, requirement21: me.requirement21, requirement31: me.requirement31, requirement41: me.requirement41, requirement51: me.requirement51, directions1: me.directions1, earlycollege: me.earlycollege, psalSportsCoed: me.psalSportsCoed, campusName: me.campusName, ptech: me.ptech, admissionspriority61: me.admissionspriority61, girls: me.girls, offerRate2: me.offerRate2, admissionspriority12: me.admissionspriority12, admissionspriority22: me.admissionspriority22, admissionspriority32: me.admissionspriority32, admissionspriority42: me.admissionspriority42, eligibility1: me.eligibility1, requirement12: me.requirement12, auditioninformation1: me.auditioninformation1, auditioninformation2: me.auditioninformation2, commonAudition1: me.commonAudition1, commonAudition2: me.commonAudition2, requirement22: me.requirement22, requirement32: me.requirement32, prgdesc4: me.prgdesc4, requirement14: me.requirement14, requirement24: me.requirement24, requirement34: me.requirement34, requirement42: me.requirement42, requirement44: me.requirement44, offerRate3: me.offerRate3, offerRate4: me.offerRate4, program4: me.program4, code4: me.code4, interest4: me.interest4, method4: me.method4, seats9Ge4: me.seats9Ge4, grade9Gefilledflag4: me.grade9Gefilledflag4, grade9Geapplicants4: me.grade9Geapplicants4, seats9Swd4: me.seats9Swd4, grade9Swdfilledflag4: me.grade9Swdfilledflag4, grade9Swdapplicants4: me.grade9Swdapplicants4, seats104: me.seats104, admissionspriority14: me.admissionspriority14, admissionspriority23: me.admissionspriority23, admissionspriority24: me.admissionspriority24, auditioninformation3: me.auditioninformation3, auditioninformation4: me.auditioninformation4, grade9Geapplicantsperseat4: me.grade9Geapplicantsperseat4, grade9Swdapplicantsperseat4: me.grade9Swdapplicantsperseat4, transfer: me.transfer, international: me.international, specialized: me.specialized, seats1Specialized: me.seats1Specialized, applicants1Specialized: me.applicants1Specialized, appperseat1Specialized: me.appperseat1Specialized, prgdesc5: me.prgdesc5, requirement15: me.requirement15, requirement25: me.requirement25, requirement35: me.requirement35, requirement45: me.requirement45, requirement52: me.requirement52, requirement54: me.requirement54, requirement55: me.requirement55, program5: me.program5, code5: me.code5, interest5: me.interest5, method5: me.method5, seats9Ge5: me.seats9Ge5, grade9Gefilledflag5: me.grade9Gefilledflag5, grade9Geapplicants5: me.grade9Geapplicants5, seats9Swd5: me.seats9Swd5, grade9Swdfilledflag5: me.grade9Swdfilledflag5, grade9Swdapplicants5: me.grade9Swdapplicants5, seats105: me.seats105, admissionspriority15: me.admissionspriority15, auditioninformation5: me.auditioninformation5, commonAudition3: me.commonAudition3, commonAudition4: me.commonAudition4, commonAudition5: me.commonAudition5, grade9Geapplicantsperseat5: me.grade9Geapplicantsperseat5, grade9Swdapplicantsperseat5: me.grade9Swdapplicantsperseat5, directions2: me.directions2, requirement62: me.requirement62, directions3: me.directions3, directions4: me.directions4, eligibility3: me.eligibility3, eligibility4: me.eligibility4, prgdesc6: me.prgdesc6, offerRate5: me.offerRate5, offerRate6: me.offerRate6, program6: me.program6, code6: me.code6, interest6: me.interest6, method6: me.method6, seats9Ge6: me.seats9Ge6, grade9Gefilledflag6: me.grade9Gefilledflag6, grade9Geapplicants6: me.grade9Geapplicants6, seats9Swd6: me.seats9Swd6, grade9Swdfilledflag6: me.grade9Swdfilledflag6, grade9Swdapplicants6: me.grade9Swdapplicants6, seats106: me.seats106, admissionspriority16: me.admissionspriority16, admissionspriority25: me.admissionspriority25, admissionspriority26: me.admissionspriority26, admissionspriority33: me.admissionspriority33, admissionspriority34: me.admissionspriority34, admissionspriority35: me.admissionspriority35, admissionspriority36: me.admissionspriority36, admissionspriority46: me.admissionspriority46, admissionspriority56: me.admissionspriority56, grade9Geapplicantsperseat6: me.grade9Geapplicantsperseat6, grade9Swdapplicantsperseat6: me.grade9Swdapplicantsperseat6, requirement61: me.requirement61, directions5: me.directions5, directions6: me.directions6, requirement16: me.requirement16, requirement26: me.requirement26, requirement36: me.requirement36, requirement46: me.requirement46, requirement56: me.requirement56, seats2Specialized: me.seats2Specialized, seats3Specialized: me.seats3Specialized, seats4Specialized: me.seats4Specialized, seats5Specialized: me.seats5Specialized, seats6Specialized: me.seats6Specialized, applicants2Specialized: me.applicants2Specialized, applicants3Specialized: me.applicants3Specialized, applicants4Specialized: me.applicants4Specialized, applicants5Specialized: me.applicants5Specialized, applicants6Specialized: me.applicants6Specialized, appperseat2Specialized: me.appperseat2Specialized, appperseat3Specialized: me.appperseat3Specialized, appperseat4Specialized: me.appperseat4Specialized, appperseat5Specialized: me.appperseat5Specialized, appperseat6Specialized: me.appperseat6Specialized, auditioninformation6: me.auditioninformation6, commonAudition6: me.commonAudition6, boys: me.boys, admissionspriority52: me.admissionspriority52, geoeligibility: me.geoeligibility, program7: me.program7, code7: me.code7, interest7: me.interest7, method7: me.method7, seats9Ge7: me.seats9Ge7, grade9Gefilledflag7: me.grade9Gefilledflag7, grade9Geapplicants7: me.grade9Geapplicants7, seats9Swd7: me.seats9Swd7, grade9Swdfilledflag7: me.grade9Swdfilledflag7, grade9Swdapplicants7: me.grade9Swdapplicants7, seats107: me.seats107, admissionspriority17: me.admissionspriority17, admissionspriority44: me.admissionspriority44, grade9Geapplicantsperseat7: me.grade9Geapplicantsperseat7, grade9Swdapplicantsperseat7: me.grade9Swdapplicantsperseat7, admissionspriority54: me.admissionspriority54, admissionspriority64: me.admissionspriority64, admissionspriority74: me.admissionspriority74, admissionspriority43: me.admissionspriority43, admissionspriority53: me.admissionspriority53, admissionspriority62: me.admissionspriority62, admissionspriority63: me.admissionspriority63, prgdesc7: me.prgdesc7, offerRate7: me.offerRate7, admissionspriority27: me.admissionspriority27, eligibility5: me.eligibility5, requirement17: me.requirement17, requirement27: me.requirement27, requirement37: me.requirement37, requirement47: me.requirement47, eligibility6: me.eligibility6, eligibility7: me.eligibility7, auditioninformation7: me.auditioninformation7, commonAudition7: me.commonAudition7, admissionspriority71: me.admissionspriority71, program8: me.program8, code8: me.code8, interest8: me.interest8, method8: me.method8, seats9Ge8: me.seats9Ge8, grade9Gefilledflag8: me.grade9Gefilledflag8, grade9Geapplicants8: me.grade9Geapplicants8, seats9Swd8: me.seats9Swd8, grade9Swdfilledflag8: me.grade9Swdfilledflag8, grade9Swdapplicants8: me.grade9Swdapplicants8, seats108: me.seats108, admissionspriority18: me.admissionspriority18, grade9Geapplicantsperseat8: me.grade9Geapplicantsperseat8, grade9Swdapplicantsperseat8: me.grade9Swdapplicantsperseat8, directions7: me.directions7, prgdesc8: me.prgdesc8, admissionspriority37: me.admissionspriority37, prgdesc9: me.prgdesc9, requirement18: me.requirement18, offerRate8: me.offerRate8, program9: me.program9, code9: me.code9, interest9: me.interest9, method9: me.method9, seats9Ge9: me.seats9Ge9, grade9Gefilledflag9: me.grade9Gefilledflag9, grade9Geapplicants9: me.grade9Geapplicants9, seats9Swd9: me.seats9Swd9, grade9Swdfilledflag9: me.grade9Swdfilledflag9, grade9Swdapplicants9: me.grade9Swdapplicants9, seats109: me.seats109, admissionspriority19: me.admissionspriority19, admissionspriority28: me.admissionspriority28, grade9Geapplicantsperseat9: me.grade9Geapplicantsperseat9, grade9Swdapplicantsperseat9: me.grade9Swdapplicantsperseat9, requirement57: me.requirement57, requirement67: me.requirement67, prgdesc10: me.prgdesc10, requirement28: me.requirement28, requirement38: me.requirement38, offerRate9: me.offerRate9, program10: me.program10, code10: me.code10, interest10: me.interest10, method10: me.method10, seats9Ge10: me.seats9Ge10, grade9Gefilledflag10: me.grade9Gefilledflag10, grade9Geapplicants10: me.grade9Geapplicants10, seats9Swd10: me.seats9Swd10, grade9Swdfilledflag10: me.grade9Swdfilledflag10, grade9Swdapplicants10: me.grade9Swdapplicants10, seats1010: me.seats1010, admissionspriority110: me.admissionspriority110, admissionspriority29: me.admissionspriority29, grade9Geapplicantsperseat10: me.grade9Geapplicantsperseat10, grade9Swdapplicantsperseat10: me.grade9Swdapplicantsperseat10)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        dbn: String? = nil,
        schoolName: String? = nil,
        boro: String? = nil,
        overviewParagraph: String? = nil,
        academicopportunities1: String?? = nil,
        academicopportunities2: String?? = nil,
        academicopportunities3: String?? = nil,
        academicopportunities4: String?? = nil,
        academicopportunities5: String?? = nil,
        ellPrograms: String? = nil,
        languageClasses: String?? = nil,
        advancedplacementCourses: String?? = nil,
        neighborhood: String? = nil,
        sharedSpace: String?? = nil,
        buildingCode: String?? = nil,
        location: String? = nil,
        phoneNumber: String? = nil,
        faxNumber: String?? = nil,
        schoolEmail: String?? = nil,
        website: String? = nil,
        subway: String? = nil,
        bus: String? = nil,
        grades2018: String? = nil,
        finalgrades: String? = nil,
        totalStudents: String? = nil,
        startTime: String?? = nil,
        endTime: String?? = nil,
        addtlInfo1: String?? = nil,
        extracurricularActivities: String?? = nil,
        psalSportsBoys: String?? = nil,
        schoolSports: String?? = nil,
        graduationRate: String?? = nil,
        attendanceRate: String? = nil,
        pctStuEnoughVariety: String?? = nil,
        collegeCareerRate: String?? = nil,
        pctStuSafe: String?? = nil,
        schoolAccessibilityDescription: String?? = nil,
        offerRate1: String?? = nil,
        program1: String? = nil,
        code1: String? = nil,
        interest1: String? = nil,
        method1: String? = nil,
        seats9Ge1: String?? = nil,
        grade9Gefilledflag1: String?? = nil,
        grade9Geapplicants1: String?? = nil,
        seats9Swd1: String?? = nil,
        grade9Swdfilledflag1: String?? = nil,
        grade9Swdapplicants1: String?? = nil,
        seats101: String? = nil,
        admissionspriority11: String?? = nil,
        admissionspriority21: String?? = nil,
        admissionspriority31: String?? = nil,
        admissionspriority41: String?? = nil,
        admissionspriority51: String?? = nil,
        grade9Geapplicantsperseat1: String?? = nil,
        grade9Swdapplicantsperseat1: String?? = nil,
        primaryAddressLine1: String? = nil,
        city: String? = nil,
        zip: String? = nil,
        stateCode: String? = nil,
        latitude: String?? = nil,
        longitude: String?? = nil,
        communityBoard: String?? = nil,
        councilDistrict: String?? = nil,
        censusTract: String?? = nil,
        bin: String?? = nil,
        bbl: String?? = nil,
        nta: String?? = nil,
        borough: String?? = nil,
        school10ThSeats: String?? = nil,
        psalSportsGirls: String?? = nil,
        prgdesc1: String?? = nil,
        prgdesc2: String?? = nil,
        prgdesc3: String?? = nil,
        requirement13: String?? = nil,
        requirement23: String?? = nil,
        requirement33: String?? = nil,
        requirement43: String?? = nil,
        requirement53: String?? = nil,
        requirement63: String?? = nil,
        program2: String?? = nil,
        program3: String?? = nil,
        code2: String?? = nil,
        code3: String?? = nil,
        interest2: String?? = nil,
        interest3: String?? = nil,
        method2: String?? = nil,
        method3: String?? = nil,
        seats9Ge2: String?? = nil,
        seats9Ge3: String?? = nil,
        grade9Gefilledflag2: String?? = nil,
        grade9Gefilledflag3: String?? = nil,
        grade9Geapplicants2: String?? = nil,
        grade9Geapplicants3: String?? = nil,
        seats9Swd2: String?? = nil,
        seats9Swd3: String?? = nil,
        grade9Swdfilledflag2: String?? = nil,
        grade9Swdfilledflag3: String?? = nil,
        grade9Swdapplicants2: String?? = nil,
        grade9Swdapplicants3: String?? = nil,
        seats102: String?? = nil,
        seats103: String?? = nil,
        admissionspriority13: String?? = nil,
        eligibility2: String?? = nil,
        grade9Geapplicantsperseat2: String?? = nil,
        grade9Geapplicantsperseat3: String?? = nil,
        grade9Swdapplicantsperseat2: String?? = nil,
        grade9Swdapplicantsperseat3: String?? = nil,
        diplomaendorsements: String?? = nil,
        pbat: String?? = nil,
        requirement11: String?? = nil,
        requirement21: String?? = nil,
        requirement31: String?? = nil,
        requirement41: String?? = nil,
        requirement51: String?? = nil,
        directions1: String?? = nil,
        earlycollege: String?? = nil,
        psalSportsCoed: String?? = nil,
        campusName: String?? = nil,
        ptech: String?? = nil,
        admissionspriority61: String?? = nil,
        girls: String?? = nil,
        offerRate2: String?? = nil,
        admissionspriority12: String?? = nil,
        admissionspriority22: String?? = nil,
        admissionspriority32: String?? = nil,
        admissionspriority42: String?? = nil,
        eligibility1: String?? = nil,
        requirement12: String?? = nil,
        auditioninformation1: String?? = nil,
        auditioninformation2: String?? = nil,
        commonAudition1: String?? = nil,
        commonAudition2: String?? = nil,
        requirement22: String?? = nil,
        requirement32: String?? = nil,
        prgdesc4: String?? = nil,
        requirement14: String?? = nil,
        requirement24: String?? = nil,
        requirement34: String?? = nil,
        requirement42: String?? = nil,
        requirement44: String?? = nil,
        offerRate3: String?? = nil,
        offerRate4: String?? = nil,
        program4: String?? = nil,
        code4: String?? = nil,
        interest4: String?? = nil,
        method4: String?? = nil,
        seats9Ge4: String?? = nil,
        grade9Gefilledflag4: String?? = nil,
        grade9Geapplicants4: String?? = nil,
        seats9Swd4: String?? = nil,
        grade9Swdfilledflag4: String?? = nil,
        grade9Swdapplicants4: String?? = nil,
        seats104: String?? = nil,
        admissionspriority14: String?? = nil,
        admissionspriority23: String?? = nil,
        admissionspriority24: String?? = nil,
        auditioninformation3: String?? = nil,
        auditioninformation4: String?? = nil,
        grade9Geapplicantsperseat4: String?? = nil,
        grade9Swdapplicantsperseat4: String?? = nil,
        transfer: String?? = nil,
        international: String?? = nil,
        specialized: String?? = nil,
        seats1Specialized: String?? = nil,
        applicants1Specialized: String?? = nil,
        appperseat1Specialized: String?? = nil,
        prgdesc5: String?? = nil,
        requirement15: String?? = nil,
        requirement25: String?? = nil,
        requirement35: String?? = nil,
        requirement45: String?? = nil,
        requirement52: String?? = nil,
        requirement54: String?? = nil,
        requirement55: String?? = nil,
        program5: String?? = nil,
        code5: String?? = nil,
        interest5: String?? = nil,
        method5: String?? = nil,
        seats9Ge5: String?? = nil,
        grade9Gefilledflag5: String?? = nil,
        grade9Geapplicants5: String?? = nil,
        seats9Swd5: String?? = nil,
        grade9Swdfilledflag5: String?? = nil,
        grade9Swdapplicants5: String?? = nil,
        seats105: String?? = nil,
        admissionspriority15: String?? = nil,
        auditioninformation5: String?? = nil,
        commonAudition3: String?? = nil,
        commonAudition4: String?? = nil,
        commonAudition5: String?? = nil,
        grade9Geapplicantsperseat5: String?? = nil,
        grade9Swdapplicantsperseat5: String?? = nil,
        directions2: String?? = nil,
        requirement62: String?? = nil,
        directions3: String?? = nil,
        directions4: String?? = nil,
        eligibility3: String?? = nil,
        eligibility4: String?? = nil,
        prgdesc6: String?? = nil,
        offerRate5: String?? = nil,
        offerRate6: String?? = nil,
        program6: String?? = nil,
        code6: String?? = nil,
        interest6: String?? = nil,
        method6: String?? = nil,
        seats9Ge6: String?? = nil,
        grade9Gefilledflag6: String?? = nil,
        grade9Geapplicants6: String?? = nil,
        seats9Swd6: String?? = nil,
        grade9Swdfilledflag6: String?? = nil,
        grade9Swdapplicants6: String?? = nil,
        seats106: String?? = nil,
        admissionspriority16: String?? = nil,
        admissionspriority25: String?? = nil,
        admissionspriority26: String?? = nil,
        admissionspriority33: String?? = nil,
        admissionspriority34: String?? = nil,
        admissionspriority35: String?? = nil,
        admissionspriority36: String?? = nil,
        admissionspriority46: String?? = nil,
        admissionspriority56: String?? = nil,
        grade9Geapplicantsperseat6: String?? = nil,
        grade9Swdapplicantsperseat6: String?? = nil,
        requirement61: String?? = nil,
        directions5: String?? = nil,
        directions6: String?? = nil,
        requirement16: String?? = nil,
        requirement26: String?? = nil,
        requirement36: String?? = nil,
        requirement46: String?? = nil,
        requirement56: String?? = nil,
        seats2Specialized: String?? = nil,
        seats3Specialized: String?? = nil,
        seats4Specialized: String?? = nil,
        seats5Specialized: String?? = nil,
        seats6Specialized: String?? = nil,
        applicants2Specialized: String?? = nil,
        applicants3Specialized: String?? = nil,
        applicants4Specialized: String?? = nil,
        applicants5Specialized: String?? = nil,
        applicants6Specialized: String?? = nil,
        appperseat2Specialized: String?? = nil,
        appperseat3Specialized: String?? = nil,
        appperseat4Specialized: String?? = nil,
        appperseat5Specialized: String?? = nil,
        appperseat6Specialized: String?? = nil,
        auditioninformation6: String?? = nil,
        commonAudition6: String?? = nil,
        boys: String?? = nil,
        admissionspriority52: String?? = nil,
        geoeligibility: String?? = nil,
        program7: String?? = nil,
        code7: String?? = nil,
        interest7: String?? = nil,
        method7: String?? = nil,
        seats9Ge7: String?? = nil,
        grade9Gefilledflag7: String?? = nil,
        grade9Geapplicants7: String?? = nil,
        seats9Swd7: String?? = nil,
        grade9Swdfilledflag7: String?? = nil,
        grade9Swdapplicants7: String?? = nil,
        seats107: String?? = nil,
        admissionspriority17: String?? = nil,
        admissionspriority44: String?? = nil,
        grade9Geapplicantsperseat7: String?? = nil,
        grade9Swdapplicantsperseat7: String?? = nil,
        admissionspriority54: String?? = nil,
        admissionspriority64: String?? = nil,
        admissionspriority74: String?? = nil,
        admissionspriority43: String?? = nil,
        admissionspriority53: String?? = nil,
        admissionspriority62: String?? = nil,
        admissionspriority63: String?? = nil,
        prgdesc7: String?? = nil,
        offerRate7: String?? = nil,
        admissionspriority27: String?? = nil,
        eligibility5: String?? = nil,
        requirement17: String?? = nil,
        requirement27: String?? = nil,
        requirement37: String?? = nil,
        requirement47: String?? = nil,
        eligibility6: String?? = nil,
        eligibility7: String?? = nil,
        auditioninformation7: String?? = nil,
        commonAudition7: String?? = nil,
        admissionspriority71: String?? = nil,
        program8: String?? = nil,
        code8: String?? = nil,
        interest8: String?? = nil,
        method8: String?? = nil,
        seats9Ge8: String?? = nil,
        grade9Gefilledflag8: String?? = nil,
        grade9Geapplicants8: String?? = nil,
        seats9Swd8: String?? = nil,
        grade9Swdfilledflag8: String?? = nil,
        grade9Swdapplicants8: String?? = nil,
        seats108: String?? = nil,
        admissionspriority18: String?? = nil,
        grade9Geapplicantsperseat8: String?? = nil,
        grade9Swdapplicantsperseat8: String?? = nil,
        directions7: String?? = nil,
        prgdesc8: String?? = nil,
        admissionspriority37: String?? = nil,
        prgdesc9: String?? = nil,
        requirement18: String?? = nil,
        offerRate8: String?? = nil,
        program9: String?? = nil,
        code9: String?? = nil,
        interest9: String?? = nil,
        method9: String?? = nil,
        seats9Ge9: String?? = nil,
        grade9Gefilledflag9: String?? = nil,
        grade9Geapplicants9: String?? = nil,
        seats9Swd9: String?? = nil,
        grade9Swdfilledflag9: String?? = nil,
        grade9Swdapplicants9: String?? = nil,
        seats109: String?? = nil,
        admissionspriority19: String?? = nil,
        admissionspriority28: String?? = nil,
        grade9Geapplicantsperseat9: String?? = nil,
        grade9Swdapplicantsperseat9: String?? = nil,
        requirement57: String?? = nil,
        requirement67: String?? = nil,
        prgdesc10: String?? = nil,
        requirement28: String?? = nil,
        requirement38: String?? = nil,
        offerRate9: String?? = nil,
        program10: String?? = nil,
        code10: String?? = nil,
        interest10: String?? = nil,
        method10: String?? = nil,
        seats9Ge10: String?? = nil,
        grade9Gefilledflag10: String?? = nil,
        grade9Geapplicants10: String?? = nil,
        seats9Swd10: String?? = nil,
        grade9Swdfilledflag10: String?? = nil,
        grade9Swdapplicants10: String?? = nil,
        seats1010: String?? = nil,
        admissionspriority110: String?? = nil,
        admissionspriority29: String?? = nil,
        grade9Geapplicantsperseat10: String?? = nil,
        grade9Swdapplicantsperseat10: String?? = nil
        ) -> SchoolElement {
        return SchoolElement(
            dbn: dbn ?? self.dbn,
            schoolName: schoolName ?? self.schoolName,
            boro: boro ?? self.boro,
            overviewParagraph: overviewParagraph ?? self.overviewParagraph,
            academicopportunities1: academicopportunities1 ?? self.academicopportunities1,
            academicopportunities2: academicopportunities2 ?? self.academicopportunities2,
            academicopportunities3: academicopportunities3 ?? self.academicopportunities3,
            academicopportunities4: academicopportunities4 ?? self.academicopportunities4,
            academicopportunities5: academicopportunities5 ?? self.academicopportunities5,
            ellPrograms: ellPrograms ?? self.ellPrograms,
            languageClasses: languageClasses ?? self.languageClasses,
            advancedplacementCourses: advancedplacementCourses ?? self.advancedplacementCourses,
            neighborhood: neighborhood ?? self.neighborhood,
            sharedSpace: sharedSpace ?? self.sharedSpace,
            buildingCode: buildingCode ?? self.buildingCode,
            location: location ?? self.location,
            phoneNumber: phoneNumber ?? self.phoneNumber,
            faxNumber: faxNumber ?? self.faxNumber,
            schoolEmail: schoolEmail ?? self.schoolEmail,
            website: website ?? self.website,
            subway: subway ?? self.subway,
            bus: bus ?? self.bus,
            grades2018: grades2018 ?? self.grades2018,
            finalgrades: finalgrades ?? self.finalgrades,
            totalStudents: totalStudents ?? self.totalStudents,
            startTime: startTime ?? self.startTime,
            endTime: endTime ?? self.endTime,
            addtlInfo1: addtlInfo1 ?? self.addtlInfo1,
            extracurricularActivities: extracurricularActivities ?? self.extracurricularActivities,
            psalSportsBoys: psalSportsBoys ?? self.psalSportsBoys,
            schoolSports: schoolSports ?? self.schoolSports,
            graduationRate: graduationRate ?? self.graduationRate,
            attendanceRate: attendanceRate ?? self.attendanceRate,
            pctStuEnoughVariety: pctStuEnoughVariety ?? self.pctStuEnoughVariety,
            collegeCareerRate: collegeCareerRate ?? self.collegeCareerRate,
            pctStuSafe: pctStuSafe ?? self.pctStuSafe,
            schoolAccessibilityDescription: schoolAccessibilityDescription ?? self.schoolAccessibilityDescription,
            offerRate1: offerRate1 ?? self.offerRate1,
            program1: program1 ?? self.program1,
            code1: code1 ?? self.code1,
            interest1: interest1 ?? self.interest1,
            method1: method1 ?? self.method1,
            seats9Ge1: seats9Ge1 ?? self.seats9Ge1,
            grade9Gefilledflag1: grade9Gefilledflag1 ?? self.grade9Gefilledflag1,
            grade9Geapplicants1: grade9Geapplicants1 ?? self.grade9Geapplicants1,
            seats9Swd1: seats9Swd1 ?? self.seats9Swd1,
            grade9Swdfilledflag1: grade9Swdfilledflag1 ?? self.grade9Swdfilledflag1,
            grade9Swdapplicants1: grade9Swdapplicants1 ?? self.grade9Swdapplicants1,
            seats101: seats101 ?? self.seats101,
            admissionspriority11: admissionspriority11 ?? self.admissionspriority11,
            admissionspriority21: admissionspriority21 ?? self.admissionspriority21,
            admissionspriority31: admissionspriority31 ?? self.admissionspriority31,
            admissionspriority41: admissionspriority41 ?? self.admissionspriority41,
            admissionspriority51: admissionspriority51 ?? self.admissionspriority51,
            grade9Geapplicantsperseat1: grade9Geapplicantsperseat1 ?? self.grade9Geapplicantsperseat1,
            grade9Swdapplicantsperseat1: grade9Swdapplicantsperseat1 ?? self.grade9Swdapplicantsperseat1,
            primaryAddressLine1: primaryAddressLine1 ?? self.primaryAddressLine1,
            city: city ?? self.city,
            zip: zip ?? self.zip,
            stateCode: stateCode ?? self.stateCode,
            latitude: latitude ?? self.latitude,
            longitude: longitude ?? self.longitude,
            communityBoard: communityBoard ?? self.communityBoard,
            councilDistrict: councilDistrict ?? self.councilDistrict,
            censusTract: censusTract ?? self.censusTract,
            bin: bin ?? self.bin,
            bbl: bbl ?? self.bbl,
            nta: nta ?? self.nta,
            borough: borough ?? self.borough,
            school10ThSeats: school10ThSeats ?? self.school10ThSeats,
            psalSportsGirls: psalSportsGirls ?? self.psalSportsGirls,
            prgdesc1: prgdesc1 ?? self.prgdesc1,
            prgdesc2: prgdesc2 ?? self.prgdesc2,
            prgdesc3: prgdesc3 ?? self.prgdesc3,
            requirement13: requirement13 ?? self.requirement13,
            requirement23: requirement23 ?? self.requirement23,
            requirement33: requirement33 ?? self.requirement33,
            requirement43: requirement43 ?? self.requirement43,
            requirement53: requirement53 ?? self.requirement53,
            requirement63: requirement63 ?? self.requirement63,
            program2: program2 ?? self.program2,
            program3: program3 ?? self.program3,
            code2: code2 ?? self.code2,
            code3: code3 ?? self.code3,
            interest2: interest2 ?? self.interest2,
            interest3: interest3 ?? self.interest3,
            method2: method2 ?? self.method2,
            method3: method3 ?? self.method3,
            seats9Ge2: seats9Ge2 ?? self.seats9Ge2,
            seats9Ge3: seats9Ge3 ?? self.seats9Ge3,
            grade9Gefilledflag2: grade9Gefilledflag2 ?? self.grade9Gefilledflag2,
            grade9Gefilledflag3: grade9Gefilledflag3 ?? self.grade9Gefilledflag3,
            grade9Geapplicants2: grade9Geapplicants2 ?? self.grade9Geapplicants2,
            grade9Geapplicants3: grade9Geapplicants3 ?? self.grade9Geapplicants3,
            seats9Swd2: seats9Swd2 ?? self.seats9Swd2,
            seats9Swd3: seats9Swd3 ?? self.seats9Swd3,
            grade9Swdfilledflag2: grade9Swdfilledflag2 ?? self.grade9Swdfilledflag2,
            grade9Swdfilledflag3: grade9Swdfilledflag3 ?? self.grade9Swdfilledflag3,
            grade9Swdapplicants2: grade9Swdapplicants2 ?? self.grade9Swdapplicants2,
            grade9Swdapplicants3: grade9Swdapplicants3 ?? self.grade9Swdapplicants3,
            seats102: seats102 ?? self.seats102,
            seats103: seats103 ?? self.seats103,
            admissionspriority13: admissionspriority13 ?? self.admissionspriority13,
            eligibility2: eligibility2 ?? self.eligibility2,
            grade9Geapplicantsperseat2: grade9Geapplicantsperseat2 ?? self.grade9Geapplicantsperseat2,
            grade9Geapplicantsperseat3: grade9Geapplicantsperseat3 ?? self.grade9Geapplicantsperseat3,
            grade9Swdapplicantsperseat2: grade9Swdapplicantsperseat2 ?? self.grade9Swdapplicantsperseat2,
            grade9Swdapplicantsperseat3: grade9Swdapplicantsperseat3 ?? self.grade9Swdapplicantsperseat3,
            diplomaendorsements: diplomaendorsements ?? self.diplomaendorsements,
            pbat: pbat ?? self.pbat,
            requirement11: requirement11 ?? self.requirement11,
            requirement21: requirement21 ?? self.requirement21,
            requirement31: requirement31 ?? self.requirement31,
            requirement41: requirement41 ?? self.requirement41,
            requirement51: requirement51 ?? self.requirement51,
            directions1: directions1 ?? self.directions1,
            earlycollege: earlycollege ?? self.earlycollege,
            psalSportsCoed: psalSportsCoed ?? self.psalSportsCoed,
            campusName: campusName ?? self.campusName,
            ptech: ptech ?? self.ptech,
            admissionspriority61: admissionspriority61 ?? self.admissionspriority61,
            girls: girls ?? self.girls,
            offerRate2: offerRate2 ?? self.offerRate2,
            admissionspriority12: admissionspriority12 ?? self.admissionspriority12,
            admissionspriority22: admissionspriority22 ?? self.admissionspriority22,
            admissionspriority32: admissionspriority32 ?? self.admissionspriority32,
            admissionspriority42: admissionspriority42 ?? self.admissionspriority42,
            eligibility1: eligibility1 ?? self.eligibility1,
            requirement12: requirement12 ?? self.requirement12,
            auditioninformation1: auditioninformation1 ?? self.auditioninformation1,
            auditioninformation2: auditioninformation2 ?? self.auditioninformation2,
            commonAudition1: commonAudition1 ?? self.commonAudition1,
            commonAudition2: commonAudition2 ?? self.commonAudition2,
            requirement22: requirement22 ?? self.requirement22,
            requirement32: requirement32 ?? self.requirement32,
            prgdesc4: prgdesc4 ?? self.prgdesc4,
            requirement14: requirement14 ?? self.requirement14,
            requirement24: requirement24 ?? self.requirement24,
            requirement34: requirement34 ?? self.requirement34,
            requirement42: requirement42 ?? self.requirement42,
            requirement44: requirement44 ?? self.requirement44,
            offerRate3: offerRate3 ?? self.offerRate3,
            offerRate4: offerRate4 ?? self.offerRate4,
            program4: program4 ?? self.program4,
            code4: code4 ?? self.code4,
            interest4: interest4 ?? self.interest4,
            method4: method4 ?? self.method4,
            seats9Ge4: seats9Ge4 ?? self.seats9Ge4,
            grade9Gefilledflag4: grade9Gefilledflag4 ?? self.grade9Gefilledflag4,
            grade9Geapplicants4: grade9Geapplicants4 ?? self.grade9Geapplicants4,
            seats9Swd4: seats9Swd4 ?? self.seats9Swd4,
            grade9Swdfilledflag4: grade9Swdfilledflag4 ?? self.grade9Swdfilledflag4,
            grade9Swdapplicants4: grade9Swdapplicants4 ?? self.grade9Swdapplicants4,
            seats104: seats104 ?? self.seats104,
            admissionspriority14: admissionspriority14 ?? self.admissionspriority14,
            admissionspriority23: admissionspriority23 ?? self.admissionspriority23,
            admissionspriority24: admissionspriority24 ?? self.admissionspriority24,
            auditioninformation3: auditioninformation3 ?? self.auditioninformation3,
            auditioninformation4: auditioninformation4 ?? self.auditioninformation4,
            grade9Geapplicantsperseat4: grade9Geapplicantsperseat4 ?? self.grade9Geapplicantsperseat4,
            grade9Swdapplicantsperseat4: grade9Swdapplicantsperseat4 ?? self.grade9Swdapplicantsperseat4,
            transfer: transfer ?? self.transfer,
            international: international ?? self.international,
            specialized: specialized ?? self.specialized,
            seats1Specialized: seats1Specialized ?? self.seats1Specialized,
            applicants1Specialized: applicants1Specialized ?? self.applicants1Specialized,
            appperseat1Specialized: appperseat1Specialized ?? self.appperseat1Specialized,
            prgdesc5: prgdesc5 ?? self.prgdesc5,
            requirement15: requirement15 ?? self.requirement15,
            requirement25: requirement25 ?? self.requirement25,
            requirement35: requirement35 ?? self.requirement35,
            requirement45: requirement45 ?? self.requirement45,
            requirement52: requirement52 ?? self.requirement52,
            requirement54: requirement54 ?? self.requirement54,
            requirement55: requirement55 ?? self.requirement55,
            program5: program5 ?? self.program5,
            code5: code5 ?? self.code5,
            interest5: interest5 ?? self.interest5,
            method5: method5 ?? self.method5,
            seats9Ge5: seats9Ge5 ?? self.seats9Ge5,
            grade9Gefilledflag5: grade9Gefilledflag5 ?? self.grade9Gefilledflag5,
            grade9Geapplicants5: grade9Geapplicants5 ?? self.grade9Geapplicants5,
            seats9Swd5: seats9Swd5 ?? self.seats9Swd5,
            grade9Swdfilledflag5: grade9Swdfilledflag5 ?? self.grade9Swdfilledflag5,
            grade9Swdapplicants5: grade9Swdapplicants5 ?? self.grade9Swdapplicants5,
            seats105: seats105 ?? self.seats105,
            admissionspriority15: admissionspriority15 ?? self.admissionspriority15,
            auditioninformation5: auditioninformation5 ?? self.auditioninformation5,
            commonAudition3: commonAudition3 ?? self.commonAudition3,
            commonAudition4: commonAudition4 ?? self.commonAudition4,
            commonAudition5: commonAudition5 ?? self.commonAudition5,
            grade9Geapplicantsperseat5: grade9Geapplicantsperseat5 ?? self.grade9Geapplicantsperseat5,
            grade9Swdapplicantsperseat5: grade9Swdapplicantsperseat5 ?? self.grade9Swdapplicantsperseat5,
            directions2: directions2 ?? self.directions2,
            requirement62: requirement62 ?? self.requirement62,
            directions3: directions3 ?? self.directions3,
            directions4: directions4 ?? self.directions4,
            eligibility3: eligibility3 ?? self.eligibility3,
            eligibility4: eligibility4 ?? self.eligibility4,
            prgdesc6: prgdesc6 ?? self.prgdesc6,
            offerRate5: offerRate5 ?? self.offerRate5,
            offerRate6: offerRate6 ?? self.offerRate6,
            program6: program6 ?? self.program6,
            code6: code6 ?? self.code6,
            interest6: interest6 ?? self.interest6,
            method6: method6 ?? self.method6,
            seats9Ge6: seats9Ge6 ?? self.seats9Ge6,
            grade9Gefilledflag6: grade9Gefilledflag6 ?? self.grade9Gefilledflag6,
            grade9Geapplicants6: grade9Geapplicants6 ?? self.grade9Geapplicants6,
            seats9Swd6: seats9Swd6 ?? self.seats9Swd6,
            grade9Swdfilledflag6: grade9Swdfilledflag6 ?? self.grade9Swdfilledflag6,
            grade9Swdapplicants6: grade9Swdapplicants6 ?? self.grade9Swdapplicants6,
            seats106: seats106 ?? self.seats106,
            admissionspriority16: admissionspriority16 ?? self.admissionspriority16,
            admissionspriority25: admissionspriority25 ?? self.admissionspriority25,
            admissionspriority26: admissionspriority26 ?? self.admissionspriority26,
            admissionspriority33: admissionspriority33 ?? self.admissionspriority33,
            admissionspriority34: admissionspriority34 ?? self.admissionspriority34,
            admissionspriority35: admissionspriority35 ?? self.admissionspriority35,
            admissionspriority36: admissionspriority36 ?? self.admissionspriority36,
            admissionspriority46: admissionspriority46 ?? self.admissionspriority46,
            admissionspriority56: admissionspriority56 ?? self.admissionspriority56,
            grade9Geapplicantsperseat6: grade9Geapplicantsperseat6 ?? self.grade9Geapplicantsperseat6,
            grade9Swdapplicantsperseat6: grade9Swdapplicantsperseat6 ?? self.grade9Swdapplicantsperseat6,
            requirement61: requirement61 ?? self.requirement61,
            directions5: directions5 ?? self.directions5,
            directions6: directions6 ?? self.directions6,
            requirement16: requirement16 ?? self.requirement16,
            requirement26: requirement26 ?? self.requirement26,
            requirement36: requirement36 ?? self.requirement36,
            requirement46: requirement46 ?? self.requirement46,
            requirement56: requirement56 ?? self.requirement56,
            seats2Specialized: seats2Specialized ?? self.seats2Specialized,
            seats3Specialized: seats3Specialized ?? self.seats3Specialized,
            seats4Specialized: seats4Specialized ?? self.seats4Specialized,
            seats5Specialized: seats5Specialized ?? self.seats5Specialized,
            seats6Specialized: seats6Specialized ?? self.seats6Specialized,
            applicants2Specialized: applicants2Specialized ?? self.applicants2Specialized,
            applicants3Specialized: applicants3Specialized ?? self.applicants3Specialized,
            applicants4Specialized: applicants4Specialized ?? self.applicants4Specialized,
            applicants5Specialized: applicants5Specialized ?? self.applicants5Specialized,
            applicants6Specialized: applicants6Specialized ?? self.applicants6Specialized,
            appperseat2Specialized: appperseat2Specialized ?? self.appperseat2Specialized,
            appperseat3Specialized: appperseat3Specialized ?? self.appperseat3Specialized,
            appperseat4Specialized: appperseat4Specialized ?? self.appperseat4Specialized,
            appperseat5Specialized: appperseat5Specialized ?? self.appperseat5Specialized,
            appperseat6Specialized: appperseat6Specialized ?? self.appperseat6Specialized,
            auditioninformation6: auditioninformation6 ?? self.auditioninformation6,
            commonAudition6: commonAudition6 ?? self.commonAudition6,
            boys: boys ?? self.boys,
            admissionspriority52: admissionspriority52 ?? self.admissionspriority52,
            geoeligibility: geoeligibility ?? self.geoeligibility,
            program7: program7 ?? self.program7,
            code7: code7 ?? self.code7,
            interest7: interest7 ?? self.interest7,
            method7: method7 ?? self.method7,
            seats9Ge7: seats9Ge7 ?? self.seats9Ge7,
            grade9Gefilledflag7: grade9Gefilledflag7 ?? self.grade9Gefilledflag7,
            grade9Geapplicants7: grade9Geapplicants7 ?? self.grade9Geapplicants7,
            seats9Swd7: seats9Swd7 ?? self.seats9Swd7,
            grade9Swdfilledflag7: grade9Swdfilledflag7 ?? self.grade9Swdfilledflag7,
            grade9Swdapplicants7: grade9Swdapplicants7 ?? self.grade9Swdapplicants7,
            seats107: seats107 ?? self.seats107,
            admissionspriority17: admissionspriority17 ?? self.admissionspriority17,
            admissionspriority44: admissionspriority44 ?? self.admissionspriority44,
            grade9Geapplicantsperseat7: grade9Geapplicantsperseat7 ?? self.grade9Geapplicantsperseat7,
            grade9Swdapplicantsperseat7: grade9Swdapplicantsperseat7 ?? self.grade9Swdapplicantsperseat7,
            admissionspriority54: admissionspriority54 ?? self.admissionspriority54,
            admissionspriority64: admissionspriority64 ?? self.admissionspriority64,
            admissionspriority74: admissionspriority74 ?? self.admissionspriority74,
            admissionspriority43: admissionspriority43 ?? self.admissionspriority43,
            admissionspriority53: admissionspriority53 ?? self.admissionspriority53,
            admissionspriority62: admissionspriority62 ?? self.admissionspriority62,
            admissionspriority63: admissionspriority63 ?? self.admissionspriority63,
            prgdesc7: prgdesc7 ?? self.prgdesc7,
            offerRate7: offerRate7 ?? self.offerRate7,
            admissionspriority27: admissionspriority27 ?? self.admissionspriority27,
            eligibility5: eligibility5 ?? self.eligibility5,
            requirement17: requirement17 ?? self.requirement17,
            requirement27: requirement27 ?? self.requirement27,
            requirement37: requirement37 ?? self.requirement37,
            requirement47: requirement47 ?? self.requirement47,
            eligibility6: eligibility6 ?? self.eligibility6,
            eligibility7: eligibility7 ?? self.eligibility7,
            auditioninformation7: auditioninformation7 ?? self.auditioninformation7,
            commonAudition7: commonAudition7 ?? self.commonAudition7,
            admissionspriority71: admissionspriority71 ?? self.admissionspriority71,
            program8: program8 ?? self.program8,
            code8: code8 ?? self.code8,
            interest8: interest8 ?? self.interest8,
            method8: method8 ?? self.method8,
            seats9Ge8: seats9Ge8 ?? self.seats9Ge8,
            grade9Gefilledflag8: grade9Gefilledflag8 ?? self.grade9Gefilledflag8,
            grade9Geapplicants8: grade9Geapplicants8 ?? self.grade9Geapplicants8,
            seats9Swd8: seats9Swd8 ?? self.seats9Swd8,
            grade9Swdfilledflag8: grade9Swdfilledflag8 ?? self.grade9Swdfilledflag8,
            grade9Swdapplicants8: grade9Swdapplicants8 ?? self.grade9Swdapplicants8,
            seats108: seats108 ?? self.seats108,
            admissionspriority18: admissionspriority18 ?? self.admissionspriority18,
            grade9Geapplicantsperseat8: grade9Geapplicantsperseat8 ?? self.grade9Geapplicantsperseat8,
            grade9Swdapplicantsperseat8: grade9Swdapplicantsperseat8 ?? self.grade9Swdapplicantsperseat8,
            directions7: directions7 ?? self.directions7,
            prgdesc8: prgdesc8 ?? self.prgdesc8,
            admissionspriority37: admissionspriority37 ?? self.admissionspriority37,
            prgdesc9: prgdesc9 ?? self.prgdesc9,
            requirement18: requirement18 ?? self.requirement18,
            offerRate8: offerRate8 ?? self.offerRate8,
            program9: program9 ?? self.program9,
            code9: code9 ?? self.code9,
            interest9: interest9 ?? self.interest9,
            method9: method9 ?? self.method9,
            seats9Ge9: seats9Ge9 ?? self.seats9Ge9,
            grade9Gefilledflag9: grade9Gefilledflag9 ?? self.grade9Gefilledflag9,
            grade9Geapplicants9: grade9Geapplicants9 ?? self.grade9Geapplicants9,
            seats9Swd9: seats9Swd9 ?? self.seats9Swd9,
            grade9Swdfilledflag9: grade9Swdfilledflag9 ?? self.grade9Swdfilledflag9,
            grade9Swdapplicants9: grade9Swdapplicants9 ?? self.grade9Swdapplicants9,
            seats109: seats109 ?? self.seats109,
            admissionspriority19: admissionspriority19 ?? self.admissionspriority19,
            admissionspriority28: admissionspriority28 ?? self.admissionspriority28,
            grade9Geapplicantsperseat9: grade9Geapplicantsperseat9 ?? self.grade9Geapplicantsperseat9,
            grade9Swdapplicantsperseat9: grade9Swdapplicantsperseat9 ?? self.grade9Swdapplicantsperseat9,
            requirement57: requirement57 ?? self.requirement57,
            requirement67: requirement67 ?? self.requirement67,
            prgdesc10: prgdesc10 ?? self.prgdesc10,
            requirement28: requirement28 ?? self.requirement28,
            requirement38: requirement38 ?? self.requirement38,
            offerRate9: offerRate9 ?? self.offerRate9,
            program10: program10 ?? self.program10,
            code10: code10 ?? self.code10,
            interest10: interest10 ?? self.interest10,
            method10: method10 ?? self.method10,
            seats9Ge10: seats9Ge10 ?? self.seats9Ge10,
            grade9Gefilledflag10: grade9Gefilledflag10 ?? self.grade9Gefilledflag10,
            grade9Geapplicants10: grade9Geapplicants10 ?? self.grade9Geapplicants10,
            seats9Swd10: seats9Swd10 ?? self.seats9Swd10,
            grade9Swdfilledflag10: grade9Swdfilledflag10 ?? self.grade9Swdfilledflag10,
            grade9Swdapplicants10: grade9Swdapplicants10 ?? self.grade9Swdapplicants10,
            seats1010: seats1010 ?? self.seats1010,
            admissionspriority110: admissionspriority110 ?? self.admissionspriority110,
            admissionspriority29: admissionspriority29 ?? self.admissionspriority29,
            grade9Geapplicantsperseat10: grade9Geapplicantsperseat10 ?? self.grade9Geapplicantsperseat10,
            grade9Swdapplicantsperseat10: grade9Swdapplicantsperseat10 ?? self.grade9Swdapplicantsperseat10
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

typealias School = [SchoolElement]

extension Array where Element == School.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(School.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers
extension DataRequest {
    func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
}

extension DataRequest {
    @discardableResult
    func responseSchool(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<School>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseSatScore(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<SatScore>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}


