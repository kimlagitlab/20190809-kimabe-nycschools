//
//  SatScoreElement.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - SatScoreElement
class SatScoreElement: Codable {
    let dbn: String
    let schoolName: String
    let numOfSatTestTakers: String
    let satCriticalReadingAvgScore: String
    let satMathAvgScore: String
    let satWritingAvgScore: String
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
    init(dbn: String, schoolName: String, numOfSatTestTakers: String, satCriticalReadingAvgScore: String, satMathAvgScore: String, satWritingAvgScore: String) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.numOfSatTestTakers = numOfSatTestTakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
}

// MARK: SatScoreElement convenience initializers and mutators

extension SatScoreElement {
    convenience init(data: Data) throws {
        let me = try newJSONDecoder().decode(SatScoreElement.self, from: data)
        self.init(dbn: me.dbn, schoolName: me.schoolName, numOfSatTestTakers: me.numOfSatTestTakers, satCriticalReadingAvgScore: me.satCriticalReadingAvgScore, satMathAvgScore: me.satMathAvgScore, satWritingAvgScore: me.satWritingAvgScore)
    }
    
    convenience init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    convenience init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func with(
        dbn: String? = nil,
        schoolName: String? = nil,
        numOfSatTestTakers: String? = nil,
        satCriticalReadingAvgScore: String? = nil,
        satMathAvgScore: String? = nil,
        satWritingAvgScore: String? = nil
        ) -> SatScoreElement {
        return SatScoreElement(
            dbn: dbn ?? self.dbn,
            schoolName: schoolName ?? self.schoolName,
            numOfSatTestTakers: numOfSatTestTakers ?? self.numOfSatTestTakers,
            satCriticalReadingAvgScore: satCriticalReadingAvgScore ?? self.satCriticalReadingAvgScore,
            satMathAvgScore: satMathAvgScore ?? self.satMathAvgScore,
            satWritingAvgScore: satWritingAvgScore ?? self.satWritingAvgScore
        )
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}

typealias SatScore = [SatScoreElement]

extension Array where Element == SatScore.Element {
    init(data: Data) throws {
        self = try newJSONDecoder().decode(SatScore.self, from: data)
    }
    
    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
        guard let data = json.data(using: encoding) else {
            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
        }
        try self.init(data: data)
    }
    
    init(fromURL url: URL) throws {
        try self.init(data: try Data(contentsOf: url))
    }
    
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
    
    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
        return String(data: try self.jsonData(), encoding: encoding)
    }
}
