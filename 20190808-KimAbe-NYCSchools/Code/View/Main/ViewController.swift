//
//  ViewController.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Moo31. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var viewHero: UIView!
    @IBOutlet weak var viewMenu1: UIView!
    @IBOutlet weak var viewMenu2: UIView!
    @IBOutlet weak var viewMenu3: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap0 = UITapGestureRecognizer(target: self, action: #selector(self.tap0_action))
        viewHero.addGestureRecognizer(tap0)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tap1_action))
        viewMenu1.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tap2_action))
        viewMenu2.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tap3_action))
        viewMenu3.addGestureRecognizer(tap3)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueDetailsViewControllerModal" {
            if let detailsViewControllerInstance = segue.destination as? DetailsViewController {
                detailsViewControllerInstance.schoolElementInstance = sender as? SchoolElement
            }
        }
    }
    
    @objc func tap0_action() {
        openWebsite(url: "https://www.schools.nyc.gov/school-life/food/summer-meals")
    }
    
    @objc func tap1_action() {
        performSegue(withIdentifier: "segueListViewController", sender: nil)
    }
    
    @objc func tap2_action() {
        openWebsite(url: "https://www.schools.nyc.gov")
    }
    
    @objc func tap3_action() {
        openWebsite(url: "https://www.schools.nyc.gov/docs/default-source/default-document-library/2019nychsdirectorycitywideenglish")
        
    }
    
    func openWebsite(url: String) {
        guard let website = URL(string: url) else { return }
        
        UIApplication.shared.open(website, options: [:], completionHandler: nil)
    }
    
}

