//
//  DetailsViewController.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import MessageUI

protocol DetailsViewControllerDelegate {
    func didFetchSatScores(satScore: SatScoreElement)
}

class DetailsViewController : UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topViewHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblFax: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblSat1: UILabel!
    @IBOutlet weak var lblSat2: UILabel!
    @IBOutlet weak var lblSat3: UILabel!
    @IBOutlet weak var lblSat4: UILabel!
    
    @IBOutlet weak var lblStats1: UILabel!
    @IBOutlet weak var lblStats2: UILabel!
    @IBOutlet weak var lblStats3: UILabel!
    
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnDirections: UIButton!
    @IBOutlet weak var btnWebsite: UIButton!
    
    var schoolElementInstance : SchoolElement?
    var schoolSatElementInstance : SatScoreElement?
    
    var viewModel : DetailsToDetailsViewController = DetailsToDetailsViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTopView()
        setupUI()
        updateUI()
        
        let emailTap = UITapGestureRecognizer(target: self, action: #selector(self.emailAction))
        lblEmail.addGestureRecognizer(emailTap)
        
        viewModel.delegate = self
        viewModel.FetchSatScores(wherex: "dbn='\(schoolElementInstance!.dbn)'")
    }
    
    func setupTopView() {
        if self.isModal {
            topView.isHidden = true
            topViewHeightContraint.constant = 0
            self.updateViewConstraints()
        } else {
            topView.isHidden = false
            topViewHeightContraint.constant = 44
            self.updateViewConstraints()
        }
    }
    
    func setupUI() {
        btnCall.layer.cornerRadius = 5
        btnCall.clipsToBounds = true
        btnDirections.layer.cornerRadius = 5
        btnDirections.clipsToBounds = true
        btnWebsite.layer.cornerRadius = 5
        btnWebsite.clipsToBounds = true
    }
    
    func updateUI() {
        guard let school = schoolElementInstance else {
            return
        }
        setMapForPlace(name: school.schoolName,
                       latitude: Double(school.latitude ?? "0")!,
                       longitude: Double(school.longitude ?? "0")!)
        
        lblName.text = school.schoolName
        
        lblAddress1.text = school.primaryAddressLine1
        lblAddress2.text = "\(school.city), \(school.neighborhood), \(school.zip)"
        
        lblPhone.text = "Phone: +1 \(school.phoneNumber)"
        lblFax.text = "Fax: +1 \(school.faxNumber ?? " ")"
        lblEmail.text = school.schoolEmail
        
        lblStats1.text = "Students Total: \(school.totalStudents)"
        lblStats2.text = "Attendance Rate: \(Int(round(Double(school.attendanceRate)! * 100)))%"
        lblStats3.text = "Graduation Rate: \(Int(round(Double(school.graduationRate ?? "1")! * 100)))%"
        
        lblOverview.text = school.overviewParagraph
    }
    
    func updateUITwo() {
        guard let satScore = schoolSatElementInstance else {
            return
        }
        
        self.lblSat1.text = "SAT Takers: \(satScore.numOfSatTestTakers) students"
        self.lblSat2.text = "Reading Avg.: \(satScore.satCriticalReadingAvgScore)"
        self.lblSat3.text = "Math Avg.: \(satScore.satMathAvgScore)"
        self.lblSat4.text = "Writing Avg.: \(satScore.satWritingAvgScore)"
    }
    
    
    @IBAction func btnCall_action(_ sender: Any) {
        call_action()
    }
    
    @IBAction func btnDirections_action(_ sender: Any) {
        guard let school = schoolElementInstance else {
            return
        }
        
        openMapForPlace(name: school.schoolName,
                        latitude: Double(school.latitude ?? "0")!,
                        longitude: Double(school.longitude ?? "0")!)
    }
    
    @IBAction func btnWebsite_action(_ sender: Any) {
        website_action()
    }
    
    @objc func emailAction() {
        guard let school = schoolElementInstance else {
            return
        }
        
        if let email = school.schoolEmail {
            sendEmail(email: email)
        }
    }
    
    @IBAction func btnClose_action(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func call_action() {
        guard let school = schoolElementInstance else { return }
        guard let number = URL(string: "tel://" + school.phoneNumber) else { return }
        
        UIApplication.shared.open(number)
    }
    
    func website_action() {
        guard let school = schoolElementInstance else { return }
        guard let website = URL(string: "http://\(school.website)") else { return }
        
        UIApplication.shared.open(website, options: [:], completionHandler: nil)
    }
    
    //Fix: Should setup a proper delegate for MapView
    func setMapForPlace(name: String, latitude: Double, longitude: Double) {
        mapview.mapType = MKMapType.standard
        
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude),
                                              longitude: CLLocationDegrees(longitude))
        
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        mapview.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = "\(name)"
        mapview.addAnnotation(annotation)
    }
    
    func openMapForPlace(name: String, latitude: Double, longitude: Double) {
        let latitude: CLLocationDegrees = latitude
        let longitude: CLLocationDegrees = longitude
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
}

extension DetailsViewController : MFMailComposeViewControllerDelegate {
    func sendEmail(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>Hello!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}

extension DetailsViewController : DetailsViewControllerDelegate {
    func didFetchSatScores(satScore: SatScoreElement) {
        self.schoolSatElementInstance = satScore
        self.updateUITwo()
    }
}

