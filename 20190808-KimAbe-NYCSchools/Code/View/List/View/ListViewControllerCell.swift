//
//  ListViewControllerCell.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import UIKit

class ListViewControllerCell : UITableViewCell {
    @IBOutlet weak var lblSchoolName: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    
    func configCell(schoolId: String, schoolName: String, schoolAddress: String, schoolBorough: String, schoolCity: String, schoolZipcode: String, color: UIColor) {
        self.lblSchoolName.text = schoolName
        self.lblSchoolName.textColor = color
        self.lblAddress1.text = schoolAddress
        self.lblAddress2.text = "\(schoolZipcode), \(schoolCity)"
    }
}
