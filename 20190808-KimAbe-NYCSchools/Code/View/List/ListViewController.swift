//
//  ListViewController.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import Foundation
import UIKit

class TableviewHeaderPosition {
    var displayed : Int = 0
    var removed : Int = 0
}

protocol ListViewControllerDelegate {
    func didFetchList(list: [[SchoolElement]], listHeader: [String], listColor: [UIColor])
}

class ListViewController : UIViewController {
    
    @IBOutlet var tableview: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var viewMenu0: UIView!
    @IBOutlet weak var viewMenu1: UIView!
    @IBOutlet weak var viewMenu2: UIView!
    @IBOutlet weak var viewMenu3: UIView!
    @IBOutlet weak var viewMenu4: UIView!
    
    var tableviewDataHeader : [String] = []
    var tableviewDataColor : [UIColor] = []
    var tableviewData : [[SchoolElement]] = []
    var loadingData : Bool = false
    var expandedSectionHeaderNumber : Int = 0
    
    var viewModel : ListToListViewController = ListToListViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Schools by Borough"
        
        let tap0 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap0(_:)))
        viewMenu0.addGestureRecognizer(tap0)
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap1(_:)))
        viewMenu1.addGestureRecognizer(tap1)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap2(_:)))
        viewMenu2.addGestureRecognizer(tap2)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap3(_:)))
        viewMenu3.addGestureRecognizer(tap3)
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(self.handleTap4(_:)))
        viewMenu4.addGestureRecognizer(tap4)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(closeTap))
        
        setupTableview()
        loadingData = true
        indicator.startAnimating()
        indicator.isHidden = false
        
        viewModel.delegate = self
        viewModel.FetchList(query: "dbn ASC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "Schools by Borough"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.title = "Schools"
        if segue.identifier == "segueDetailsViewController" {
            if let detailsViewControllerInstance = segue.destination as? DetailsViewController {
                detailsViewControllerInstance.schoolElementInstance = sender as? SchoolElement
            }
        }
    }
    
    fileprivate func setupTableview() {
        self.tableview.delegate = self
        self.tableview.dataSource = self
        self.tableview.rowHeight = UITableView.automaticDimension
        self.tableview.estimatedRowHeight = 80.0
        self.tableview.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.tableview.register(UINib(nibName: "ListViewControllerCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        self.tableview.tableFooterView = UIView(frame: .zero)
    }
    
    fileprivate func updateUI() {
        tableview.reloadData()
    }
    
    //A cleaner tap handler can be implemented here
    @objc func handleTap0(_ sender: UITapGestureRecognizer? = nil) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 0)
            self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func handleTap1(_ sender: UITapGestureRecognizer? = nil) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 1)
            self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func handleTap2(_ sender: UITapGestureRecognizer? = nil) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 2)
            self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func handleTap3(_ sender: UITapGestureRecognizer? = nil) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 3)
            self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    @objc func handleTap4(_ sender: UITapGestureRecognizer? = nil) {
        DispatchQueue.main.async {
            let indexPath = IndexPath(item: 0, section: 4)
            self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
        }
    }
    
    @objc func closeTap() {
        dismiss(animated: true, completion: nil)
    }
    
    
}

extension ListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = UIView(frame: CGRect(x: 1, y: 50, width: 276, height: 60))
        headerView.backgroundColor = tableviewDataColor[section]
        let labelView: UILabel = UILabel(frame: CGRect(x: 16, y: 20, width: 276, height: 30))
        labelView.text = tableviewDataHeader[section]
        labelView.font = UIFont.boldSystemFont(ofSize: 30)
        labelView.textColor = .white
        headerView.addSubview(labelView)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableviewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ListViewControllerCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListViewControllerCell
        
        cell.configCell(schoolId: self.tableviewData[indexPath.section][indexPath.row].dbn,
                        schoolName: self.tableviewData[indexPath.section][indexPath.row].schoolName,
                        schoolAddress: self.tableviewData[indexPath.section][indexPath.row].primaryAddressLine1,
                        schoolBorough: self.tableviewData[indexPath.section][indexPath.row].borough ?? "",
                        schoolCity: self.tableviewData[indexPath.section][indexPath.row].nta ?? "",
                        schoolZipcode: self.tableviewData[indexPath.section][indexPath.row].zip,
                        color: self.tableviewDataColor[indexPath.section])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolElementInstance = self.tableviewData[indexPath.section][indexPath.row]
        self.performSegue(withIdentifier: "segueDetailsViewController", sender: schoolElementInstance)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplayingHeaderView view: UIView, forSection section: Int) {
        print("OUT:", section)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        print("IN:", section)
    }
    
}

extension ListViewController : ListViewControllerDelegate {
    func didFetchList(list: [[SchoolElement]], listHeader: [String], listColor: [UIColor]) {
        self.tableviewDataHeader = listHeader
        self.tableviewDataColor = listColor
        self.tableviewData = list
        self.loadingData = false
        indicator.stopAnimating()
        indicator.isHidden = true
        self.tableview.reloadData()
    }
}
