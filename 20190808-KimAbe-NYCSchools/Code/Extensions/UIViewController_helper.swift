//
//  UIViewController_helper.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var isModal: Bool {
        let presentingIsModal = presentingViewController != nil
        return presentingIsModal
    }
}

