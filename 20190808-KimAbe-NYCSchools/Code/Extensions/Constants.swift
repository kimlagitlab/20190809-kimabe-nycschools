//
//  Constants.swift
//  20190808-KimAbe-NYCSchools
//
//  Created by Kim Abe on 8/9/19.
//  Copyright © 2019 Kim. All rights reserved.
//


import Foundation
import UIKit

struct Kilo {
    struct ProductionServer {
        static let baseURL = "https://data.cityofnewyork.us/resource"
    }
    
    struct APIParameterKey {
        /*static let password = "password"
         static let email = "email"
         static let offset = "offset"
         static let limit = "limit"*/
        static let query = "$query"
        static let order = "$order"
        static let wherex = "$where"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

enum boroughColor: String {
    case bronx = "672C91"
    case brooklyn = "DD1350"
    case queens = "F36F21"
    case statenisland = "006EB8"
    case manhattan = "2DB44A"
    
    func color() -> UIColor {
        return colorFromHex(self.rawValue)
    }
}
